package com.example.jade_wine_front.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    private Boolean success;

    private String message;

    private String code;

    private Object data;

    public static Result success() {
        return success(null);
    }

    public static Result success(Object data) {
        return new Result(true, "执行成功", "2000", data);
    }

    public static Result success(String code, String message) {
        return new Result(false, message, code, null);
    }

    public static Result fail(String code, String message) {
        return new Result(false, message, code, null);
    }

    public static Result fail(String message) {
        return fail("9999", message);
    }

    public static Result fail() {
        return fail("9999", "执行失败");
    }
}