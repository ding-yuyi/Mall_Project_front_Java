package com.example.jade_wine_front.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result handleException(Exception e) {
        log.warn("出现异常:" + e.getMessage(), e);
        return Result.fail("服务器开了个小差，程序员小哥正在努力修复，请稍等。错误提示：" + e.getMessage());
    }

}
