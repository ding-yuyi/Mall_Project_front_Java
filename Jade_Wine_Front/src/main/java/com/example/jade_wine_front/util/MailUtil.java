package com.example.jade_wine_front.util;

import lombok.extern.slf4j.Slf4j;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @PackageName:
 * @ClassName: mailUtil
 * @Description:
 * @author: 丁予一
 * @date: 2021/5/14 14:43
 */
@Slf4j
public class MailUtil {
    /**
     * 发送邮件的方法
     */
    public static void sendEmail(String origin, String password, String target, String subject, String emailMsg) throws MessagingException {
        log.info("开始发送邮件");
        //创建session象,需要两个参数，properties和authenticator
        //创建properties对象，此对象是对session参数的一些配置
        Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol", "SMTP");
        properties.setProperty("mail.host", "smtp.163.com");//163邮箱配置，如果是QQ邮箱，则需要更改为
        properties.setProperty("mail.smtp.auth", "true");

        //创建authenticator对象，即验证器对象
        Authenticator authenticator = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(origin, password);
            }
        };

        Session session = Session.getInstance(properties, authenticator);//两个参数：properties   authenticator

        //Message对象
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(origin));//设定邮件发送者
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(target));
        message.setSubject(subject);

        message.setContent(emailMsg, "text/html;charset=utf-8");

        //发送邮件
        Transport.send(message);
    }
}
