package com.example.jade_wine_front.util;

import java.util.Random;

/**
 * @PackageName:
 * @ClassName: MathUtil
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/11 10:29
 */
public class MathUtil {
    //随机生成四个数字，用于验证码的生成
    public static String getVerification() {
        Random random = new Random();
        return random.nextInt(10) + "" + random.nextInt(10) + random.nextInt(10) + random.nextInt(10);
    }


    //随机生成8位的字符串，用于用户名的生成
    public static String getUsername() {
        int maxNum = 36;
        int i;
        int count = 0;
        char[] str = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
                'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        StringBuffer pwd = new StringBuffer("");
        Random random = new Random();
        while (count < 8) {
            i = Math.abs(random.nextInt(maxNum));
            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();
    }
}
