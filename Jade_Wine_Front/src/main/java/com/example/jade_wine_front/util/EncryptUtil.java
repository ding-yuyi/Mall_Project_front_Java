package com.example.jade_wine_front.util;

import org.apache.shiro.crypto.hash.SimpleHash;

/**
 * @PackageName:
 * @ClassName: EncryptUtil
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/9 16:26
 */
public class EncryptUtil {
    //加密算法
    public static final String ALGORITHM_NAME = "SHA-256";
    //迭代次数
    public static final int ITERATIONS = 1;

    public static String encrypt(String source) {
        //创建一个hash工具，设置了算法及原始数据
        SimpleHash simpleHash = new SimpleHash(ALGORITHM_NAME, source);
        //设置迭代次数
        simpleHash.setIterations(ITERATIONS);
        //转换为16进制
        return simpleHash.toHex();

    }
}
