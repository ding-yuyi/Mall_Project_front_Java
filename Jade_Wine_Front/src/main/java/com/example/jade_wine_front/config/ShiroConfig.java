package com.example.jade_wine_front.config;

import com.example.jade_wine_front.component.JWTFilter;
import com.example.jade_wine_front.component.ShiroRealm;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.LinkedHashMap;

/**
 * @PackageName:
 * @ClassName: ShiroConfig
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/9 17:00
 */
@Configuration
public class ShiroConfig {
    @Bean
    public Realm shiroRealm() {
        ShiroRealm shiroRealm = new ShiroRealm();
        return shiroRealm;
    }
    

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        DefaultShiroFilterChainDefinition sfcd = new DefaultShiroFilterChainDefinition();

        LinkedHashMap<String, Filter> filters = new LinkedHashMap<>();
        filters.put("jwt", new JWTFilter());
        shiroFilterFactoryBean.setFilters(filters);

        sfcd.addPathDefinition("/", "anon");
        sfcd.addPathDefinition("/user/user", "anon");
        sfcd.addPathDefinition("/user/email", "anon");
        sfcd.addPathDefinition("/user/verification", "anon");
        sfcd.addPathDefinition("/goods/**", "anon");
        sfcd.addPathDefinition("/pay/**", "anon");
        //还需要添加可放行的请求

        //登出
        sfcd.addPathDefinition("/logout", "logout");
        //其他则需要认证
        sfcd.addPathDefinition("/**", "jwt");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(sfcd.getFilterChainMap());
        return shiroFilterFactoryBean;
    }
}
