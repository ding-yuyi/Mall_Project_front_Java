package com.example.jade_wine_front.config;

import lombok.Data;

import java.io.FileWriter;
import java.io.IOException;

@Data
public class AlipayConfig {
//    //因为是在沙箱环境下进行测试，所以下面的信息填的值都是沙箱应用里面的值
//    // 商户appid 测试环境用的都是沙箱应用里面的值，正式环境换成你自己申请的,比如我申请的(我的第一个支付中心)里面对应的值
//    public static String APPID = "2021000117661878";
//    // 私钥 pkcs8格式的 因为公钥我用的是自己申请的那个(我的第一个支付中心)里面的那个公钥，所以私钥填的也是我的第一个中心里面对应公钥生成的私钥
//    //公钥类似于一把锁，私钥类似于钥匙，一个公钥对应一个私钥，1对1关系
//    public static String RSA_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCP0S9OFBIC6elNK6Lo2b2UouCLKq1qeGeo78Y+zw+SDx8DU+BG5PUgyZtd1r46cZeqw4lMCxNmw0rLr4kKnTFNkI4s80a+K1zOEvGjBD+istrfZkwSRDohVXBg4vOW7HmZu3tuG007G2OEfv1nKz8Kem1LKGBY79RC5UBKPlR6H74jVHXrkv4y/rNPe+ksWj42UYboxfb066r8F2+LqinF9hTujYlJ9fL+WkXImDhIEsR0xn3SA+x4OQzpqw6oYKxSi+zpPr+1pQpFlaDzm7fOI/lkKVSDjhhNJKiHH6aPOPSXZWVBFAdbkd1ttfj6A2OpT+O/2KmhKwW4ZHxIEuyfAgMBAAECggEAZXgCt0Tj3Fhq+b6i50TRRHCSmr3WsrsW8BOUPLZc1wGCMRfZsktSQ/raep/LgXKpvprNYPCjlKqPOBy2RN3Zbk9icIaIUB5KET8+a3TQ7Rrh4CLN40lt9hzQ+BrIIKDhvryPfoUWvXA6i74JTeQuS1dLi26GY9l6WmRjMr69XZe9gzIqe3LOeMVrYbbHRlpQfyr5IUW6d/A4VU7vML+zwLLKl3NTeBh686of3ZjA3+fY8eaKfZOfN6yFXMv09HZB4imR7qmc/YIZsn7SPwbbcXyL3C1IoTJGak9nsa4j5pg74gB/P268znIn6HwBn9jL7/iq/MPS+SNsCsugDQsSQQKBgQDVeOzCmGDn7PqglA9hvtlot3OTbbt2v7GyshTIe25yeAUTO5BeKlzwjr81O8Z3pRs3d7YN0e+u69Gb/4leH+7G41a6icj0QND42WQfTYy1k8Tx3QjAh3zBHY9rvEkHzxPjPpuEAJR8mw3nAWbfPvEr540PZcgT2mu/vDnfwxV5IQKBgQCsd9kVURiCUGzPmKu2CGpbV5gmwgTk0XNCc9Y22jQg2WKRmIOa4qzDaH8aRgyKnSCo41sT6W8xw+clCSaL4ohqpnKwirPMpWHwQYg7GTdcfg9XYbHLc6Irt0hKteOr/scmeP31vLmmughSEekHpkYfjBkxXgp/FShhxXItKqrtvwKBgB3/DP3XHZ7ZXOpdg7/PaotdY98uhy4Lh7O9dFArj/yDwurN7t5cg4zF8jZPvPao+6cSqkFM46ontt99y9avFAVcgp5ZqCQyS2r8WSZQ5lnJdt4pmgY7w5r7RWD1Jynyvi+rZ3zn+/V6cDyqpMLv/EGYGukz+yZXBdb+Yv9/UTAhAoGAV/2dR37kXjfiC9f8YxbQDGIYQk3iaUti3phyxfF1fvzpiRairjbPpbJ+hk0OwPmBXCkCopoKHP2xw/dzLxMYPjFFekSJjvMhkI4ejuhCknHOKeqx5vbL2jncLCG4UlveBwbbqq/ql+F5tUVl+n0ecaHsiH9OI6ALxjYSFnEaW8kCgYEAr/iwQrFOvkkH8sqY9RKi65wGFZ8TjpIACaxk5t8zo25C4AJKr0vh1NGQXxdF56ptjaLqw2p5bxNm3rduGWuGo/2Kc7XsuGkJuogbh7mosKwzoiWOQ8KOXjOKDU8hHEyhNqq8zKTvE3oepC0IPY3VR/re+6xivuf1hDNjCtLzYaQ=";
//    // 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
//    //这里我没填，因为我主要目的是学习怎么支付，所以notify_url和return_url就没管，这2个并不影响支付过程
//    public static String notify_url = "http://ki8ntb.natappfree.cc/pay/returnNtf";
//    // 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
//    public static String return_url = "http://ki8ntb.natappfree.cc/pay/returnCtl";
//    // 请求网关地址 填写沙箱应用里面的 这是正式环境的请求网址https://openapi.alipay.com/gateway.do,下方是沙箱环境的
//    public static String URL = "https://openapi.alipaydev.com/gateway.do";
//    // 编码
//    public static String CHARSET = "UTF-8";
//    // 返回格式
//    public static String FORMAT = "json";
//    // 支付宝公钥 这里我填的是沙箱应用里面RSA2(SHA256)密钥(推荐)这个生成的支付宝的公钥,正式环境是接口加签方式里面的支付宝公钥
//    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgLm367UkOwrD7WXe0Yu71Ctj8HScvnz7sJMxxBALRyDSw3Dvsd0B44Sl0pav3t2AIG71XKoAYf1HIrOqZeNWxonW/jUutLtRsckaxpq3XLcKURQaKozXDdMMJdFmQHUbItJoQLb21k4sw7u1bXV6xcUPEM6fkMAEmnRRazXagmcqxeI2TNnxaaFCheMCfk684GuMPLZZokXIjNMl9pfpcMeunfGMjxxH3yMfKmJeZLzzh+ApVOKgPOl5CsMegKaksMeDxWOuF+EsjqqWioC0ni02gkV6OobT1P8uZ+krqF8/TSKHaGKf6116nLetYixXcK540fRqaA6ykwGaGHuoTwIDAQAB";
//    // 日志记录目录 日志生成位置,E盘下必须要有先有这个文件夹，不然会报错
//    public static String log_path = "E:/logs/";
//    // RSA2
//    public static String SIGNTYPE = "RSA2";
//
//    /**
//     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
//     *
//     * @param sWord 要写入日志里的文本内容
//     */
//    public static void logResult(String sWord) {
//        FileWriter writer = null;
//        try {
//            writer = new FileWriter(log_path + "alipay_log_"
//                    + System.currentTimeMillis() + ".txt");
//            writer.write(sWord);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
    //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000117661878";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCP0S9OFBIC6elNK6Lo2b2UouCLKq1qeGeo78Y+zw+SDx8DU+BG5PUgyZtd1r46cZeqw4lMCxNmw0rLr4kKnTFNkI4s80a+K1zOEvGjBD+istrfZkwSRDohVXBg4vOW7HmZu3tuG007G2OEfv1nKz8Kem1LKGBY79RC5UBKPlR6H74jVHXrkv4y/rNPe+ksWj42UYboxfb066r8F2+LqinF9hTujYlJ9fL+WkXImDhIEsR0xn3SA+x4OQzpqw6oYKxSi+zpPr+1pQpFlaDzm7fOI/lkKVSDjhhNJKiHH6aPOPSXZWVBFAdbkd1ttfj6A2OpT+O/2KmhKwW4ZHxIEuyfAgMBAAECggEAZXgCt0Tj3Fhq+b6i50TRRHCSmr3WsrsW8BOUPLZc1wGCMRfZsktSQ/raep/LgXKpvprNYPCjlKqPOBy2RN3Zbk9icIaIUB5KET8+a3TQ7Rrh4CLN40lt9hzQ+BrIIKDhvryPfoUWvXA6i74JTeQuS1dLi26GY9l6WmRjMr69XZe9gzIqe3LOeMVrYbbHRlpQfyr5IUW6d/A4VU7vML+zwLLKl3NTeBh686of3ZjA3+fY8eaKfZOfN6yFXMv09HZB4imR7qmc/YIZsn7SPwbbcXyL3C1IoTJGak9nsa4j5pg74gB/P268znIn6HwBn9jL7/iq/MPS+SNsCsugDQsSQQKBgQDVeOzCmGDn7PqglA9hvtlot3OTbbt2v7GyshTIe25yeAUTO5BeKlzwjr81O8Z3pRs3d7YN0e+u69Gb/4leH+7G41a6icj0QND42WQfTYy1k8Tx3QjAh3zBHY9rvEkHzxPjPpuEAJR8mw3nAWbfPvEr540PZcgT2mu/vDnfwxV5IQKBgQCsd9kVURiCUGzPmKu2CGpbV5gmwgTk0XNCc9Y22jQg2WKRmIOa4qzDaH8aRgyKnSCo41sT6W8xw+clCSaL4ohqpnKwirPMpWHwQYg7GTdcfg9XYbHLc6Irt0hKteOr/scmeP31vLmmughSEekHpkYfjBkxXgp/FShhxXItKqrtvwKBgB3/DP3XHZ7ZXOpdg7/PaotdY98uhy4Lh7O9dFArj/yDwurN7t5cg4zF8jZPvPao+6cSqkFM46ontt99y9avFAVcgp5ZqCQyS2r8WSZQ5lnJdt4pmgY7w5r7RWD1Jynyvi+rZ3zn+/V6cDyqpMLv/EGYGukz+yZXBdb+Yv9/UTAhAoGAV/2dR37kXjfiC9f8YxbQDGIYQk3iaUti3phyxfF1fvzpiRairjbPpbJ+hk0OwPmBXCkCopoKHP2xw/dzLxMYPjFFekSJjvMhkI4ejuhCknHOKeqx5vbL2jncLCG4UlveBwbbqq/ql+F5tUVl+n0ecaHsiH9OI6ALxjYSFnEaW8kCgYEAr/iwQrFOvkkH8sqY9RKi65wGFZ8TjpIACaxk5t8zo25C4AJKr0vh1NGQXxdF56ptjaLqw2p5bxNm3rduGWuGo/2Kc7XsuGkJuogbh7mosKwzoiWOQ8KOXjOKDU8hHEyhNqq8zKTvE3oepC0IPY3VR/re+6xivuf1hDNjCtLzYaQ=";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgLm367UkOwrD7WXe0Yu71Ctj8HScvnz7sJMxxBALRyDSw3Dvsd0B44Sl0pav3t2AIG71XKoAYf1HIrOqZeNWxonW/jUutLtRsckaxpq3XLcKURQaKozXDdMMJdFmQHUbItJoQLb21k4sw7u1bXV6xcUPEM6fkMAEmnRRazXagmcqxeI2TNnxaaFCheMCfk684GuMPLZZokXIjNMl9pfpcMeunfGMjxxH3yMfKmJeZLzzh+ApVOKgPOl5CsMegKaksMeDxWOuF+EsjqqWioC0ni02gkV6OobT1P8uZ+krqF8/TSKHaGKf6116nLetYixXcK540fRqaA6ykwGaGHuoTwIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://wv7mag.natappfree.cc/pay/returnNtf";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://wv7mag.natappfree.cc/pay/returnCtl";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "E:/logs/";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     *
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis() + ".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}