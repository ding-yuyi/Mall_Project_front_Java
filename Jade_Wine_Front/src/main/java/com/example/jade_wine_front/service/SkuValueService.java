package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.entity.SkuValue;

/**
 * <p>
 * sku属性值表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface SkuValueService extends IService<SkuValue> {

}
