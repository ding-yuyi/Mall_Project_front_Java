package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.entity.Refund;

/**
 * <p>
 * 退款表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface RefundService extends IService<Refund> {

}
