package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.entity.OrdersDetail;
import com.example.jade_wine_front.mapper.OrdersDetailMapper;
import com.example.jade_wine_front.service.OrdersDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单详情表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-12
 */
@Service
public class OrdersDetailServiceImpl extends ServiceImpl<OrdersDetailMapper, OrdersDetail> implements OrdersDetailService {

}
