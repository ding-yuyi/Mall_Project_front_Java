package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.entity.Cart;
import com.example.jade_wine_front.entity.Orders;
import com.example.jade_wine_front.entity.OrdersDetail;
import com.example.jade_wine_front.mapper.CartMapper;
import com.example.jade_wine_front.mapper.OrdersDetailMapper;
import com.example.jade_wine_front.mapper.OrdersMapper;
import com.example.jade_wine_front.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 * 购物车表 服务实现类
 * </p>
 `
 * @author cyx
 * @since 2021-07-09
 */
@Service
@Slf4j
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements CartService {

    @Resource
    private CartMapper cartMapper;

    @Resource
    private OrdersMapper ordersMapper;

    @Resource
    private OrdersDetailMapper ordersDetailMapper;

    //根据用户查询购物车的方法
    @Override
//    @Cacheable(cacheNames = "cartCache", key = "#userId")
    public List<Cart> cartByUser(Integer userId) {
        log.info("impl查询购物车{}", userId);
        //QueryWrapper，构建查询条件
//        QueryWrapper<Cart> cartQueryWrapper = new QueryWrapper<>();
//        cartQueryWrapper.eq("user_id", userId);
        return cartMapper.cartByUser(userId);
    }

    //修改购物车中商品数量的方法
    @Override
//    @CachePut(cacheNames = "cartCache", key = "#cart.cartId")
    public Integer cartProductNum(Cart cart) {
        log.info("impl修改购物车数量{}", cart);
        //构建修改条件
        QueryWrapper<Cart> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("cart_id", cart.getCartId());
        return cartMapper.update(cart, objectQueryWrapper);
    }

    //删除购物车信息的方法
    @Override
//    @CacheEvict(cacheNames = "cartCache", key = "#cartId")
    public Integer cartInfo(Integer cartId) {
        log.info("impl删除购物车信息{}", cartId);
        QueryWrapper<Cart> cartQueryWrapper = new QueryWrapper<>();
        cartQueryWrapper.eq("cart_id", cartId);
        return cartMapper.delete(cartQueryWrapper);
    }

    //订单提交页把购物车信息存入订单和订单详情表中的method
    @Override
    public Result cart2Order(List<Cart> cartList, BigDecimal price, Integer addressId) {
        log.info("impl购物车存入订单入参{}", cartList);
        //存入订单所属的用户id和地址
        Orders orders = new Orders();
        orders.setUserId(cartList.get(0).getUserId());
        orders.setAddressId(addressId);
        orders.setOrderTotal(price);
        //生成订单号
        String s = UUID.randomUUID().toString().replace("-", "") + System.currentTimeMillis();
        orders.setOrderNum(s);
        int insert = ordersMapper.insert(orders);
        log.info("impl购物车存入订单新建订单返回结果{}", insert);

        //循环遍历form集合，操作每一个cart数据
        for (Cart cart : cartList) {
            OrdersDetail ordersDetail = new OrdersDetail();
            ordersDetail.setOrderId(orders.getOrderId());
            ordersDetail.setSkuId(cart.getSkuId());
            ordersDetail.setSkuNum(cart.getSkuNum());
            int insert1 = ordersDetailMapper.insert(ordersDetail);
            //删除购物车中的那一条数据
            cartMapper.deleteById(cart.getCartId());
            log.info("impl遍历存入订单详情返回结果{}", insert1);
        }
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("orderNum", s);
        hashMap.put("orderPrice", price.toString());
        return Result.success(hashMap);
    }

    //详情页面加入购物车的方法
    @Override
    public Integer newCartInfo(Cart cart) {
        log.info("impl详情页面加入购物车入参{}", cart);
        return cartMapper.insert(cart);
    }
}
