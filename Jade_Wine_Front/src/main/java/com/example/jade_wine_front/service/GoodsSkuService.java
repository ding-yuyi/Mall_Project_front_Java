package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.controller.form.*;
import com.example.jade_wine_front.entity.GoodsSku;

/**
 * <p>
 * sku属性键表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface GoodsSkuService extends IService<GoodsSku> {
    /**
     * 根据id查询商品详情
     */
    public GoodsSku goodsSkuById(int skuId);


    /**
     * 联合查询goods_sku,goods_spu,category3张表
     */
    GoodsSkuSpu goodsSkuSpu(Integer skuId);

    //分类查询
    Page<GoodsSku> findByCategory(FindByCategoryForm form);

    //根据名字查询
    Page<GoodsSku> queryGoodsfindByName(FindByNameForm form);

    //分页展示查询
    ReturnFindCountForm findCount(FindCountForm form);
}
