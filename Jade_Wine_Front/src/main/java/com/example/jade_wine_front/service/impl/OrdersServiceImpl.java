package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.controller.form.AddOrdersForm;
import com.example.jade_wine_front.controller.form.FindOrderByNameForm;
import com.example.jade_wine_front.controller.form.PageQueryOrdersByIdForm;
import com.example.jade_wine_front.controller.form.UpdateOrderStatueByOrderIdForm;
import com.example.jade_wine_front.entity.Orders;
import com.example.jade_wine_front.entity.OrdersAndOrderDetail;
import com.example.jade_wine_front.mapper.OrdersMapper;
import com.example.jade_wine_front.service.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
@Slf4j
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

    @Resource
    private OrdersMapper ordersMapper;

    /**
     * 根据用户ID对订单进行分页查询
     * 参数:  pageNum 页码  pageSize 当页条数   userId  用户id
     */
    @Override
    public Result pageQueryOrdersById(PageQueryOrdersByIdForm form) {
        log.info("根据用户id进行订单分页查询{}", form);
        Page<Orders> ordersPage = new Page<>(form.getPageNum(), form.getPageSize());
        IPage<Orders> ordersIPage = ordersMapper.queryOrdersByUserId(ordersPage, form.getUserId());
        log.info("返回订单{}", ordersIPage);
        if (null != ordersIPage) {
            return Result.success(ordersIPage);
        }
        return Result.fail();
    }

    /**
     * 订单状态修改
     * 参数：   orderId 订单id   orderStatue 订单状态    addressId 地址id
     */

    @Override
    public Result updateOrderStatueAndAddressByOrderId(UpdateOrderStatueByOrderIdForm form) {
        log.info("修改订单状态或地址功能{}", form);
        Orders orders = new Orders();
        BeanUtils.copyProperties(form, orders);
        UpdateWrapper<Orders> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("order_id", form.getOrderId());
        updateWrapper.set(orders.getOrderState() != null, "order_state", orders.getOrderState());
        updateWrapper.set(orders.getAddressId() != null, "address_id", orders.getAddressId());
        int update = ordersMapper.update(orders, updateWrapper);
        log.info("修改订单状态或地址是否成功{}", update);
        if (update == 1) {
            return Result.success();
        }
        return Result.fail();
    }


    /**
     * 新增订单
     * 参数： userId 用户id  orderNum 商品数量   orderState 订单状态     addressId 地址id
     */
    @Override
    public Result addOrder(AddOrdersForm form) {
        log.info("新增订单功能{}", form);
        Orders orders = new Orders();
        BeanUtils.copyProperties(form, orders);
        int insert = ordersMapper.insert(orders);
        log.info("新增订单是否成功{}", insert);
        if (insert == 1) {
            return Result.success();
        }
        return Result.fail();
    }

    /**
     * 根据订单id删除订单
     * 参数:  orderId 订单id
     */
    @Override
    public Result deleteOrderById(Integer orderId) {
        log.info("根据订单id删除订单{}", orderId);
        int i = ordersMapper.deleteById(orderId);
        log.info("根据订单id删除订单是否成功{}", i);
        if (i == 1) {
            return Result.success();
        }
        return Result.fail();
    }

    /**
     * 用户个人中心展示自己的订单功能
     * 参数： userId 用户id  pageNum 页码  page 页容量
     */
    @Override
    public Result queryOrdersAndOrderDetailByUserId(PageQueryOrdersByIdForm form) {
        log.info("用户我的订单{}", form);
        Page<OrdersAndOrderDetail> ordersPage = new Page<>(form.getPageNum(), form.getPageSize());
        IPage<OrdersAndOrderDetail> ordersIPage = ordersMapper.queryOrdersAndOrderDetailByUserId(ordersPage, form.getUserId());
        log.info("用户订单{}", ordersIPage.getRecords());
        if (null != ordersIPage) {
            return Result.success(ordersIPage);
        }
        return Result.fail();
    }

    @Override
    public Result findOrderByName(FindOrderByNameForm form) {
        log.info("根据用户id和商品名模糊查询{}", form);
        Page<OrdersAndOrderDetail> orderPage = new Page<>(form.getPageNum(), form.getPageSize());
        IPage<OrdersAndOrderDetail> ordersIPage = ordersMapper.findOrderByName(orderPage, form.getUserId(), form.getSkuName());
        log.info("用户订单{}", ordersIPage.getRecords());
        if (null != ordersIPage) {
            return Result.success(ordersIPage);
        }
        return Result.fail();
    }

    @Override
    public Integer queryOrderIdByOrderNum(String orderNum) {
        log.info("根据订单编号查询订单id{}", orderNum);
        //编写查询规则
//        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
//        ordersQueryWrapper.eq("order_num", orderNum);
        return ordersMapper.orderByOrderNum(orderNum).getOrderId();
    }
}
