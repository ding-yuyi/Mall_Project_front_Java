package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.controller.form.AddOrdersForm;
import com.example.jade_wine_front.controller.form.FindOrderByNameForm;
import com.example.jade_wine_front.controller.form.PageQueryOrdersByIdForm;
import com.example.jade_wine_front.controller.form.UpdateOrderStatueByOrderIdForm;
import com.example.jade_wine_front.entity.Orders;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface OrdersService extends IService<Orders> {
    /**
     * 根据用户ID对订单进行分页查询
     * 参数:  pageNum 页码  pageSize 当页条数   userId  用户id
     */
    public Result pageQueryOrdersById(PageQueryOrdersByIdForm form);

    /**
     * 订单状态修改
     * 参数：   orderId 订单id   orderStatue 订单状态    addressId 地址id
     */
    public Result updateOrderStatueAndAddressByOrderId(UpdateOrderStatueByOrderIdForm form);

    /**
     * 新增订单
     * 参数： userId 用户id  orderNum 商品数量   orderState 订单状态     addressId 地址id
     */
    public Result addOrder(AddOrdersForm form);

    /**
     * 根据订单id删除订单
     * 参数:  orderId 订单id
     */
    public Result deleteOrderById(Integer orderId);

    /**
     * 用户个人中心展示自己的订单功能
     * 参数： userId 用户id  pageNum 页码  page 页容量
     */
    public Result queryOrdersAndOrderDetailByUserId(PageQueryOrdersByIdForm form);

    /**
     * 用户个人中心模糊订单查询
     * 参数： userId 用户id  pageNum 页码  page 页容量 skuName模糊查询商品名
     */
    public Result findOrderByName(FindOrderByNameForm form);

    /*
     * @Author kevin
     * @Description 根据订单号查询对应的订单id
     * @Date  2021/7/14
     * @Param orderNum 订单号
     * @Return Result
     * @MethodName queryOrderIdByOrderNum
     */
    public Integer queryOrderIdByOrderNum(String orderNum);
}
