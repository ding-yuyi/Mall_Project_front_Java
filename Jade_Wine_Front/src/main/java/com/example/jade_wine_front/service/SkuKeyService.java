package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.entity.SkuKey;

/**
 * <p>
 * sku属性键表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface SkuKeyService extends IService<SkuKey> {

}
