package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.entity.OrdersDetail;

/**
 * <p>
 * 订单详情表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-12
 */
public interface OrdersDetailService extends IService<OrdersDetail> {

}
