package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.entity.Address;
import com.example.jade_wine_front.mapper.AddressMapper;
import com.example.jade_wine_front.service.AddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 用户地址表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements AddressService {

    @Resource
    private AddressMapper addressMapper;

    //根据userId查询对应地址信息的方法
    @Override
    public List<Address> addressByUser(Integer userId) {
        //构建查询结构
        QueryWrapper<Address> addressQueryWrapper = new QueryWrapper<>();
        addressQueryWrapper.eq("user_id", userId);
        addressMapper.selectList(addressQueryWrapper);
        return addressMapper.selectList(addressQueryWrapper);
    }
}
