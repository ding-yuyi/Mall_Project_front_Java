package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.entity.SkuKey;
import com.example.jade_wine_front.mapper.SkuKeyMapper;
import com.example.jade_wine_front.service.SkuKeyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku属性键表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
public class SkuKeyServiceImpl extends ServiceImpl<SkuKeyMapper, SkuKey> implements SkuKeyService {

}
