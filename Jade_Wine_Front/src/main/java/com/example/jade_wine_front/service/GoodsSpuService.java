package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.controller.form.PageQueryGoodsSpuByIdForm;
import com.example.jade_wine_front.entity.GoodsSpu;

/**
 * <p>
 * spu商品表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface GoodsSpuService extends IService<GoodsSpu> {

    /**
     * 查询所有商品(搜索框)
     */

    public Result queryGoodsSpuByAll(PageQueryGoodsSpuByIdForm form);

    /**
     * 根据商品ID进行唯一查询
     * 参数:  pageNum 页码  pageSize 当页条数  spu_id
     *
     * @param
     * @return
     */
    public GoodsSpu findGoodsSpu(int spuId);


    /**
     * 根据名字查询商品并且分页
     *
     * @param form
     */
    public Result queryGoodsfindByName(PageQueryGoodsSpuByIdForm form);

    /**
     * 类别查询并且分页
     *
     * @param form
     * @return
     */
    public Result queryGoodsfindByCount(PageQueryGoodsSpuByIdForm form);
}
