package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.entity.Warehouse;
import com.example.jade_wine_front.mapper.WarehouseMapper;
import com.example.jade_wine_front.service.WarehouseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 仓库中间表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
public class WarehouseServiceImpl extends ServiceImpl<WarehouseMapper, Warehouse> implements WarehouseService {

}
