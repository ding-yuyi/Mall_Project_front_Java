package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.controller.form.PageQueryGoodsSpuByIdForm;
import com.example.jade_wine_front.entity.GoodsSpu;
import com.example.jade_wine_front.mapper.GoodsSpuMapper;
import com.example.jade_wine_front.service.GoodsSpuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * spu商品表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
@Slf4j
public class GoodsSpuServiceImpl extends ServiceImpl<GoodsSpuMapper, GoodsSpu> implements GoodsSpuService {

    @Resource
    private GoodsSpuMapper goodsSpuMapper;

    /**
     * 查询所有商品分页查询
     * @param form
     * @return
     */
    @Override
    public Result queryGoodsSpuByAll(PageQueryGoodsSpuByIdForm form) {
        log.info("查询所有商品分页查询{}", form);
        Page<GoodsSpu> goodsSpuPage = new Page<>(form.getPageNum(), form.getPageSize());
        goodsSpuMapper.queryGoodsSpuByAll(goodsSpuPage, form.getSpu_name());
        if (null != goodsSpuPage) {
            return Result.success(goodsSpuPage);
        }
        return Result.fail();
    }


    /**
     * 通过ID唯一查询
     * @param
     * @return
     */
    @Override
    public GoodsSpu findGoodsSpu(int spuId) {
        return goodsSpuMapper.findGoodsSpu(spuId);
    }


    /**
     * 通过商品名字分查询
     * @param form
     * @return
     */
    @Override
    public Result queryGoodsfindByName(PageQueryGoodsSpuByIdForm form) {
        log.info("通过商品名字分页查询{}", form);
        Page<GoodsSpu> goodsSpuPage = new Page<>(form.getPageNum(), form.getPageSize());
        goodsSpuMapper.queryGoodsSpuByAll(goodsSpuPage, form.getSpu_name());
        if (null != goodsSpuPage) {
            return Result.success(goodsSpuPage);
        }
        return Result.fail();
    }


    /**
     * 根据商品类别名字分页查询
     */
    @Override
    public Result queryGoodsfindByCount(PageQueryGoodsSpuByIdForm form) {
        log.info("通过商品名字分页查询{}", form);
        Page<GoodsSpu> goodsSpuPage = new Page<>(form.getPageNum(), form.getPageSize());
        goodsSpuMapper.queryGoodsSpuByAll(goodsSpuPage, form.getSpu_name());
        if (null != goodsSpuPage) {
            return Result.success(goodsSpuPage);
        }
        return Result.fail();
    }


}