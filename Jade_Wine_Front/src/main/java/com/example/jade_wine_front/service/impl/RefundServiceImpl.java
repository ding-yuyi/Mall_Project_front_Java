package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.entity.Refund;
import com.example.jade_wine_front.mapper.RefundMapper;
import com.example.jade_wine_front.service.RefundService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退款表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
public class RefundServiceImpl extends ServiceImpl<RefundMapper, Refund> implements RefundService {

}
