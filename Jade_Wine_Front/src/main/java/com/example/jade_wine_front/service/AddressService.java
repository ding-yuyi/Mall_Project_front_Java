package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.entity.Address;

import java.util.List;

/**
 * <p>
 * 用户地址表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface AddressService extends IService<Address> {

    //根据用户id查询所属地址信息的方法
    public List<Address> addressByUser(Integer userId);

}
