package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.entity.Cart;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 购物车表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface CartService extends IService<Cart> {

    //根据用户查询购物车
    public List<Cart> cartByUser(Integer userId);

    //修改指定购物车条目的商品数量,数量为手动输入
    public Integer cartProductNum(Cart cart);

    //删除指定购物车条目
    public Integer cartInfo(Integer cartId);

    //购物车转换成订单数据的方法
    public Result cart2Order(List<Cart> cartList, BigDecimal price, Integer addressId);

    //加入购物车的方法
    public Integer newCartInfo(Cart cart);
}
