package com.example.jade_wine_front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_front.controller.form.LoginForm;
import com.example.jade_wine_front.controller.form.RegisterForm;
import com.example.jade_wine_front.controller.form.UpdateUserDetailForm;
import com.example.jade_wine_front.entity.User;
import com.example.jade_wine_front.entity.UserDetail;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface UserService extends IService<User> {

    Boolean register(RegisterForm form) throws InvocationTargetException, IllegalAccessException, MessagingException;

    User login(LoginForm form);

    //修改用户信息
    Boolean updateUserDetail(UpdateUserDetailForm form, Integer userId);

    //查询用户详情
    UserDetail getUserDetail(Integer userId);

//    Boolean usernameIsExits(String username);

    Boolean mailIsExits(String email);

    Boolean uploadHeadImage(MultipartFile file) throws IOException;

    Boolean checkVerification(String verification, String mail);

    String sendVerificationCode(String mail) throws MessagingException;

    String updateEmail(String oldEmail, String newEmail, String verification);

    Boolean addTel(Integer userId, String tel);

    //注销用户
    Boolean deleteUser(Integer userId);

    //修改密码
    Boolean updatePwd(Integer userId, String oldPassword, String newPassword);
    //Boolean updateUser(UpdateUserForm form, String userId) throws InvocationTargetException, IllegalAccessException;
}
