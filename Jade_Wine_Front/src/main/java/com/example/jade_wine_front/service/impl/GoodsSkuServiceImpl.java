package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.controller.form.*;
import com.example.jade_wine_front.entity.GoodsSku;
import com.example.jade_wine_front.entity.GoodsSpu;
import com.example.jade_wine_front.mapper.GoodsSkuMapper;
import com.example.jade_wine_front.mapper.GoodsSpuMapper;
import com.example.jade_wine_front.service.GoodsSkuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * sku属性键表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
@Slf4j
public class GoodsSkuServiceImpl extends ServiceImpl<GoodsSkuMapper, GoodsSku> implements GoodsSkuService {
    @Resource
    private GoodsSkuMapper goodsSkuMapper;

    @Resource
    private GoodsSpuMapper goodsSpuMapper;


    @Override
    public GoodsSku goodsSkuById(int skuId) {
        return goodsSkuMapper.goodsSkuById(skuId);
    }


    @Override
    public Page<GoodsSku> findByCategory(FindByCategoryForm form) {
        log.info("进入service,{}", form);

        Page<GoodsSku> skuPage = new Page<>(form.getPageIdx(), form.getPageSize());

        QueryWrapper<GoodsSku> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id", form.getCategoryId());
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectList(wrapper);

        Page<GoodsSku> goodsSkuPage = goodsSkuMapper.selectPage(skuPage, wrapper);
        return goodsSkuPage;
    }

    @Override
    public Page<GoodsSku> queryGoodsfindByName(FindByNameForm form) {
        log.info("进入service,{}", form);
        Page<GoodsSku> skuPage = new Page<>(form.getPageNum(), form.getPageSize());

        QueryWrapper<GoodsSku> wrapper = new QueryWrapper<>();
        wrapper.like("sku_name", form.getSkuName());

        Page<GoodsSku> goodsSkuPage = goodsSkuMapper.selectPage(skuPage, wrapper);
        log.info("查询到的商品,{}", goodsSkuPage.getRecords());
        return goodsSkuPage;
    }

    @Override
    public ReturnFindCountForm findCount(FindCountForm form) {
        log.info("查询信息,{}", form);

        Page<GoodsSpu> spuPage = new Page<>(form.getPageIdx(), form.getPageSize());
        //先查spu，再根据spuid查询sku
        QueryWrapper<GoodsSpu> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id", form.getCategoryId())
                .eq(!StringUtils.isEmpty(form.getCapacity()), "capacity", form.getCapacity())
                .eq(!StringUtils.isEmpty(form.getAlcoholContent()), "alcohol_content", form.getAlcoholContent())
                .eq(!StringUtils.isEmpty(form.getType()), "type", form.getType())
                .eq(!StringUtils.isEmpty(form.getSpuOriginPlace()), "spu_origin_place", form.getSpuOriginPlace());

        Page<GoodsSpu> page = goodsSpuMapper.selectPage(spuPage, wrapper);
        List<GoodsSpu> goodsSpus = page.getRecords();

        List<Integer> spuIds = new ArrayList<>();

        for (int i = 0; i < goodsSpus.size(); i++) {
            spuIds.add(goodsSpus.get(i).getSpuId());
        }

        log.info("spuId,{}", spuIds);
        List<GoodsSku> goodsSkus = goodsSkuMapper.findCount(spuIds);

        log.info("查询到的商品,{}", goodsSkus);

        ReturnFindCountForm returnFindCountForm = new ReturnFindCountForm();
        returnFindCountForm.setCurrent(page.getCurrent());
        returnFindCountForm.setSize(page.getSize());
        returnFindCountForm.setTotal(page.getTotal());
        returnFindCountForm.setGoodsSkus(goodsSkus);
//        goodsSkuMapper;
        return returnFindCountForm;
    }


    /**
     * 联合查询3张表goods_sku goods_spu category
     *
     * @param
     * @return
     */
    @Override
    public GoodsSkuSpu goodsSkuSpu(Integer spuId) {
        GoodsSkuSpu goodsSkuSpu = goodsSkuMapper.goodsSkuSpu(spuId);
        log.info("goodsSkuSpu:{}", goodsSkuSpu);
        return goodsSkuSpu;
    }


}
