package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.entity.WarehouseDetail;
import com.example.jade_wine_front.mapper.WarehouseDetailMapper;
import com.example.jade_wine_front.service.WarehouseDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 仓库中间表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
public class WarehouseDetailServiceImpl extends ServiceImpl<WarehouseDetailMapper, WarehouseDetail> implements WarehouseDetailService {

}
