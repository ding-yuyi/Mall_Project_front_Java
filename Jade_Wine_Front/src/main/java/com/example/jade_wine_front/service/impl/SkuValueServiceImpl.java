package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.entity.SkuValue;
import com.example.jade_wine_front.mapper.SkuValueMapper;
import com.example.jade_wine_front.service.SkuValueService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku属性值表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
public class SkuValueServiceImpl extends ServiceImpl<SkuValueMapper, SkuValue> implements SkuValueService {

}
