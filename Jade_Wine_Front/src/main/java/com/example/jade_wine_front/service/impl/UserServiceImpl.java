package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.controller.form.LoginForm;
import com.example.jade_wine_front.controller.form.RegisterForm;
import com.example.jade_wine_front.controller.form.UpdateUserDetailForm;
import com.example.jade_wine_front.entity.User;
import com.example.jade_wine_front.entity.UserDetail;
import com.example.jade_wine_front.mapper.UserDetailMapper;
import com.example.jade_wine_front.mapper.UserMapper;
import com.example.jade_wine_front.service.UserService;
import com.example.jade_wine_front.util.MailUtil;
import com.example.jade_wine_front.util.MathUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private UserDetailMapper userDetailMapper;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Boolean register(RegisterForm form) throws InvocationTargetException, IllegalAccessException, MessagingException {
        log.info("service层，用户信息,{}", form);
        //适配user
        User user = new User();
        user.setPassword(form.getPassword());
        user.setEmail(form.getEmail());
        user.setUsername(form.getEmail());
        int i = userMapper.insert(user);

        log.info("用户信息,{}", user);
        //新增userDetail
        UserDetail userDetail = new UserDetail();
        userDetail.setUserId(user.getUserId());
        userDetail.setNickname(MathUtil.getUsername());

        int j = userDetailMapper.insert(userDetail);

        if (i > 0 && j > 0) {
            return true;
        }
        return false;
    }

    @Override
    public User login(LoginForm form) {
        log.info("开始执行service层,{}", form);
        //根据用户名查找
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", form.getUsername());
        User user = userMapper.selectOne(queryWrapper);
        if (null == user) {
            //根据用邮箱查找
            QueryWrapper<User> queryWrapper1 = new QueryWrapper<>();
            queryWrapper.eq("username", form.getUsername());
            user = userMapper.selectOne(queryWrapper);
            if (null == user) {
                //根据手机查找
                QueryWrapper<User> queryWrapper2 = new QueryWrapper<>();
                queryWrapper.eq("username", form.getUsername());
                user = userMapper.selectOne(queryWrapper);
            }
        }
        log.info("用户信息，{}", user);

        //如果用户不存在，抛出异常
        if (null == user) {
            return null;
        } else {
            if (user.getPassword().equals(form.getPassword()) && user.getRole() == 0) {
                return user;
            } else {
                return null;
            }
        }
    }

    @Override
    public Boolean updateUserDetail(UpdateUserDetailForm form, Integer userId) {
        log.info("用户修改信息:{},用户id:{}", form, userId);

        //适配
        QueryWrapper<UserDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        UserDetail userDetail = userDetailMapper.selectOne(queryWrapper);
        //如果用户不存在则抛出异常

        log.info("修改前的用户详情,{}", userDetail);
        BeanUtils.copyProperties(form, userDetail);
        log.info("修改后的用户详情,{}", userDetail);

        int i = userDetailMapper.updateById(userDetail);

        if (i > 0) {
            return true;
        }
        return false;
    }

    @Override
    public UserDetail getUserDetail(Integer userId) {
        log.info("查询用户ID,{}", userId);
        QueryWrapper<UserDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        UserDetail userDetail = userDetailMapper.selectOne(queryWrapper);
        return userDetail;
    }

//    @Override
//    public Boolean usernameIsExits(String username) {
//        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
//        userQueryWrapper.eq("username",username);
//        User user = userMapper.selectOne(userQueryWrapper);
//
//        if (null == user) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public Boolean mailIsExits(String email) {


        if (null == email || email.equals("")) {
            return false;
        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("email", email);
        User user = userMapper.selectOne(queryWrapper);

        if (null == user) {
            return false;
        }
        return true;
    }

    @Override
    public Boolean uploadHeadImage(MultipartFile sourceFile) throws IOException {
        log.info("开始上传头像");
        //存储头像的路径
        File file = new File("D:\\IDEA\\IDEA_Code\\woniuMall\\web\\img");
        //新文件名称
        String fieldName = sourceFile.getName();
        String suffix = fieldName.substring(fieldName.lastIndexOf("."));
        String s = UUID.randomUUID().toString().replace("-", "");
        String saveFileName = System.currentTimeMillis() + "" + s + suffix;
        System.out.println("图片名称" + saveFileName);
        File saveFile = new File(file, saveFileName);
        sourceFile.transferTo(saveFile);

        return true;
    }

    @Override
    public Boolean checkVerification(String verification, String email) {
        log.info("开始查询,{},{}", verification, email);

        //开始校验验证码,从缓存中取
        String key = "email:" + email;
        log.info("key：{}", key);
        Boolean aBoolean = stringRedisTemplate.hasKey(key);
        log.info("有没有key:{}", aBoolean);
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        String verificationInRedis = ops.get(key);
        log.info("查询的缓存中的数据,{}", verificationInRedis);
        //验证码过期或者不正确则验证失败
        if (null == verificationInRedis || !verificationInRedis.equals(verification)) {
            log.info("进来了，验证码为空");
            return false;
        }
        return true;
    }

    @Override
    public String sendVerificationCode(String email) throws MessagingException {
        log.info("准备发送验证码");
        //随机生成四个数字的验证码
        String verification = MathUtil.getVerification();
        log.info("用户验证码：" + verification);

        //存入缓存
        String key = "email:" + email;
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        ops.set(key, verification, 60 * 3, TimeUnit.SECONDS);

        MailUtil.sendEmail("dingyi5201995@163.com", "RMWDMNTGPWWJQJCY", email, "验证码", "验证码是：" + verification);

        return verification;
    }

    @Override
    public String updateEmail(String oldEmail, String newEmail, String verification) {
        log.info("进入service");

        //查询用户，查看邮箱是否存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("email", oldEmail);
        User user = userMapper.selectOne(queryWrapper);
        log.info("查询的用户,{}", user);
        //如果用户为空，抛出异常

        //判断验证码是否正确
        String key = "email:" + newEmail;
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        String verificationInRedis = ops.get(key);
        log.info("发送的验证码,{}", verificationInRedis);
        if (null == verificationInRedis || !verificationInRedis.equals(verification)) {
            return "验证码不正确";
        }

        //验证码正确修改邮箱
        user.setEmail(newEmail);
        int i = userMapper.updateById(user);
        System.out.println(user.getEmail());
        if (i > 0) {
            return "修改成功";
        }
        return "修改失败";

    }

    @Override
    public Boolean addTel(Integer userId, String tel) {
        log.info("用户id:{},手机:{}", userId, tel);

        User user = userMapper.selectById(userId);
        log.info("用户:{}", user);

        user.setTel(tel);

        int i = userMapper.updateById(user);

        if (i > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean deleteUser(Integer userId) {
        log.info("用户id,{}", userId);
        //删除user
        int i = userMapper.deleteById(userId);

        //删除详情
        QueryWrapper<UserDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        int j = userDetailMapper.delete(queryWrapper);

        if (i > 0 && j > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean updatePwd(Integer userId, String oldPassword, String newPassword) {
        log.info("用户ID,{},用户旧密码,{},用户新密码,{}", userId, oldPassword, newPassword);

        //根据id查询用户
        User user = userMapper.selectById(userId);

        //如果没有用户，抛出异常

        log.info("修改前的用户信息:{}", user);
        //判断原密码对不对
        if (!oldPassword.equals(user.getPassword())) {
            return false;
        }

        user.setPassword(newPassword);

        int i = userMapper.updateById(user);

        if (i > 0) {
            return true;
        }
        return false;
    }

//    @Override
//    public Boolean updateUser(UpdateUserForm form, String userId) throws InvocationTargetException, IllegalAccessException {
//        log.info("进入service层,{},userId:{}", form, userId);
//        //适配
//
//        int i = -1;
//        int j = -1;
//        //如果user表中有修改的字段
//        if (null != form.getPassword() || null != form.getEmail() || null != form.getTel()) {
//            User user = new User();
//            BeanUtils.copyProperties(form, user);
//            user.setUserId(Integer.parseInt(userId));
//            i = userMapper.updateById(user);
//        }
//        //如果userDetail表中有修改的字段
//        if (null != form.getUserDetailBirthday() || null != form.getUserDetailGender() || null != form.getUserDetailIntro()) {
//            UserDetail userDetail = userDetailMapper.selectById(Integer.parseInt(userId));
//            BeanUtils.copyProperties(form, userDetail);
//            j = userDetailMapper.updateById(userDetail);
//        }
//
//        if (j > 0 || i > 0) {
//            return true;
//        }
//        return false;
//    }
}
