package com.example.jade_wine_front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_front.entity.Comment;
import com.example.jade_wine_front.mapper.CommentMapper;
import com.example.jade_wine_front.service.CommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * spu商品表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

}
