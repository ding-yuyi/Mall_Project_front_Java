package com.example.jade_wine_front.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jade_wine_front.controller.form.GoodsSkuSpu;
import com.example.jade_wine_front.entity.GoodsSku;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * sku属性键表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface GoodsSkuMapper extends BaseMapper<GoodsSku> {

    /**
     *通过id查询一条sku数据
     */
    @Select("select * from goods_sku where sku_id=#{sku_id}")
    public GoodsSku goodsSkuById(int skuId);


    /**
     * 联合查询通过 sku id查询(关联3张表)
     */
    @Select("select sku_id,sku_name,category_name,spu_brand,spu_origin_place,spu_description,spu_price,spu_stock,src,type,alcohol_content " +
            "from goods_spu g1 join goods_sku g2 on g1.spu_id=g2.spu_id join category c on c.category_id = g1.category_id where sku_id=#{skuId}")
    GoodsSkuSpu goodsSkuSpu(Integer skuId);


//    //分页展示查询
//    @Select("select sku_id,sku_name,category_name,spu_brand,spu_description,spu_price,spu_stock,src,type,alcohol_content " +
//            "from goods_spu g1 join goods_sku g2 on g1.spu_id=g2.spu_id where sku_id=#{skuId}")
//    Page<FindCountForm> findCount(Page page,FindCountForm form);

    @Select("<script> select sku_id,sku_name,spu_price,src" +
            "  FROM goods_sku" +
            "  WHERE sku_id in" +
            "  <foreach item=\"item\" index=\"index\" collection=\"spuIds\"" +
            "      open=\"(\" separator=\",\" close=\")\">" +
            "        #{item}" +
            "  </foreach>" +
            "</script>")
    List<GoodsSku> findCount(List<Integer> spuIds);
}
