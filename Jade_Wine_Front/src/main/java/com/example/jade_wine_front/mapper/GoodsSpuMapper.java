package com.example.jade_wine_front.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.jade_wine_front.entity.GoodsSpu;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * spu商品表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface GoodsSpuMapper extends BaseMapper<GoodsSpu> {

    /**
     * 根据唯一ID查询商品
     *
     * @return
     */
    @Select("select spu_id,category_id,spu_name,spu_attribute_list,spu_origin_place,spu_brand,spu_description \" +\n" +
            "            \"from goods_spu where spu_id=#{spu_id};")
    public GoodsSpu findGoodsSpu(int spuId);

    /**
     * 查询全部并分页
     *
     * @param page
     * @param spu_name
     * @return
     */
    @Select("select * from goods_spu;")
    public IPage<GoodsSpu> queryGoodsSpuByAll(Page<GoodsSpu> page, Integer spu_name);

    //根据名字查询并分页
    @Select("select spu_id,category_id,spu_name,spu_attribute_list,spu_origin_place,spu_brand,spu_description " +
            "from goods_spu where spu_name=#{spu_name};")
    public IPage<GoodsSpu> queryGoodsfindByName(Page<GoodsSpu> page, Integer spu_name);

    /**
     * 根据类别查询并分页
     */

    @Select("select spu_name,count(spu_name) from goods_spu group by spu_name;")
    public IPage<GoodsSpu> queryGoodsfindByCount(Page<GoodsSpu> page, Integer spu_name);


}
