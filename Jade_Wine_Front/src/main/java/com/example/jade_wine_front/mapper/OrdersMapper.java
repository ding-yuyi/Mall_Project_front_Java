package com.example.jade_wine_front.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.jade_wine_front.entity.Orders;
import com.example.jade_wine_front.entity.OrdersAndOrderDetail;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface OrdersMapper extends BaseMapper<Orders> {
    /**
     * 根据userId查询orders
     */
    @Select("select order_id,user_id,order_num,order_state,address_id,order_total,raw_add_time,raw_update_time from orders where user_id = #{userId}")
    public IPage<Orders> queryOrdersByUserId(Page<Orders> page, Integer userId);

    /**
     * 根据订单id查询订单以及订单详情
     */
    @Select("select o.order_id,user_id,order_num,order_state,address_id,order_total,orders_detail_id,gsku.sku_id,sku_num,gspu.spu_id,sku_attribute_specs,sku_key,spu_price,spu_stock,cg.category_id,sku_name,spu_attribute_list,spu_origin_place,spu_brand,spu_description,parent_id,category_name from orders o join orders_detail od on o.order_id = od.order_id join goods_sku gsku on od.sku_id = gsku.sku_id join goods_spu gspu on gsku.spu_id = gspu.spu_id join category cg on gspu.category_id = cg.category_id where user_id = #{userId}")
    public IPage<OrdersAndOrderDetail> queryOrdersAndOrderDetailByUserId(Page<OrdersAndOrderDetail> page, Integer userId);

    //根据订单号查询订单id
    @Select("select order_id from orders where order_num = #{order_num};")
    public Orders orderByOrderNum(String orderNum);

    //更具订单商品名和所属用户id查询订单信息
    @Select("select o.order_id,user_id,order_num,order_state,address_id,order_total,orders_detail_id,gsku.sku_id,sku_num,gspu.spu_id,sku_attribute_specs,sku_key,spu_price,spu_stock,cg.category_id,sku_name,spu_attribute_list,spu_origin_place,spu_brand,spu_description,parent_id,category_name from orders o join orders_detail od on o.order_id = od.order_id join goods_sku gsku on od.sku_id = gsku.sku_id join goods_spu gspu on gsku.spu_id = gspu.spu_id join category cg on gspu.category_id = cg.category_id where user_id = #{userId} and sku_name like '%${sku_name}%'")
    public IPage<OrdersAndOrderDetail> findOrderByName(Page<OrdersAndOrderDetail> page, Integer userId, String sku_name);
}
