package com.example.jade_wine_front.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jade_wine_front.entity.Comment;

/**
 * <p>
 * spu商品表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface CommentMapper extends BaseMapper<Comment> {

}
