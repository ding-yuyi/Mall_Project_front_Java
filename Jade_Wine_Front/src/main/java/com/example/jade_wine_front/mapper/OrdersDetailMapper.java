package com.example.jade_wine_front.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jade_wine_front.entity.OrdersDetail;

/**
 * @Title: OrdersDetailMapper
 * @ProjectName Jade_Wine_Front
 * @Description: TODO
 * @Author kevin
 * @Date 2021/7/10 14:19
 * @Version: 1.0
 */
public interface OrdersDetailMapper extends BaseMapper<OrdersDetail> {
}
