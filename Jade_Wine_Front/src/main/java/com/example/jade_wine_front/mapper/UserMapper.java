package com.example.jade_wine_front.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jade_wine_front.entity.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface UserMapper extends BaseMapper<User> {

}
