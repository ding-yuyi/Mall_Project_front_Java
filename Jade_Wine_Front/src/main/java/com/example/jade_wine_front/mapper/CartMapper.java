package com.example.jade_wine_front.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jade_wine_front.entity.Cart;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 购物车表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
public interface CartMapper extends BaseMapper<Cart> {

    //根据用户查询出购物车信息，包含spu表和sku表数据
    @Select("select user_id,sku_num,sku_name,spu_price,cart_id,cart.sku_id from cart join goods_sku on cart.sku_id=goods_sku.sku_id join goods_spu on goods_sku.spu_id=goods_spu.spu_id where user_id=#{user_id};")
    public List<Cart> cartByUser(Integer userId);

}
