package com.example.jade_wine_front;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@MapperScan("com.example.jade_wine_front.mapper")
@EnableCaching
public class JadeWineFrontApplication {

    public static void main(String[] args) {
        SpringApplication.run(JadeWineFrontApplication.class, args);
    }

}
