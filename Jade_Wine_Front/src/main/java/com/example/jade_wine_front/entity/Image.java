package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 图片表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("image")
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "image_id", type = IdType.AUTO)
    private Integer imageId;

    /**
     * 图片路径
     */
    private String imageSrc;

    /**
     * 订单id
     */
    private Integer spuId;

    /**
     * 记录添加时间
     */
    private Date rawAddTime;

    /**
     * 记录修改时间
     */
    private Date rawUpdateTime;


}
