package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * sku属性键表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sku_key")
public class SkuKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "sku_key_id", type = IdType.AUTO)
    private Integer skuKeyId;

    /**
     * 分类id
     */
    private Integer categoryId;

    /**
     * 选项名称
     */
    private String skuKey;

    /**
     * 记录添加时间
     */
    private Date rawAddTime;

    /**
     * 记录更新时间
     */
    private Date rawUpdateTime;


}
