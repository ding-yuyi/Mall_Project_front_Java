package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * sku属性值表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sku_value")
public class SkuValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "sku_value_id", type = IdType.AUTO)
    private Integer skuValueId;

    /**
     * 选项id
     */
    private Integer skuKeyId;

    /**
     * 具体属性
     */
    private String skuValue;

    /**
     * 记录添加时间
     */
    private Date rawAddTime;

    /**
     * 记录更新时间
     */
    private Date rawUpdateTime;


}
