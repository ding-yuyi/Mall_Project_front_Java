package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 仓库中间表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("warehouse")
public class Warehouse implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "warehouse_detail_id", type = IdType.AUTO)
    private Integer warehouseDetailId;

    /**
     * 仓库id
     */
    private Integer warehouseId;

    /**
     * skuid
     */
    private String skuId;

    /**
     * 库存量
     */
    private Integer stock;

    /**
     * 记录添加时间
     */
    private Date rawAddTime;

    /**
     * 记录更新时间
     */
    private Date rawUpdateTime;


}
