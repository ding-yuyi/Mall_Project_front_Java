package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * @Title: OrdersDetail
 * @ProjectName Jade_Wine_Front
 * @Description: 订单详情表类
 * @Author kevin
 * @Date 2021/7/10 13:04
 * @Version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("orders_detail")
public class OrdersDetail {

    //主键
    private Integer ordersDetailId;

    //商品id
    private Integer skuId;

    //商品数量
    private Integer skuNum;

    //订单表id
    private Integer orderId;

    /**
     * 新增时间
     */
    private Date rawAddTime;

    /**
     * 修改时间
     */
    private Date rawUpdateTime;

    private List<GoodsSku> goodsSkus;
}
