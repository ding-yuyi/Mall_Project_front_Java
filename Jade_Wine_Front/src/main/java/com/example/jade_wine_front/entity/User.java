package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户状态是否激活 | 0 未激活 | 1已激活
     */
    private Integer userStatus;

    /**
     * 激活码
     */
    private String activeCode;

    /**
     * 电话
     */
    private String tel;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 角色 | 0 普通用户 | 1 管理员
     */
    private Integer role;

    /**
     * 最后登陆时间
     */
    private Date userLastLoginTime;

    /**
     * 记录添加时间
     */
    private Date rawAddTime;

    /**
     * 记录修改时间
     */
    private Date rawUpdateTime;


}
