package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 退款表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("refund")
public class Refund implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "refund_id", type = IdType.AUTO)
    private Integer refundId;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 退款原因
     */
    private String refundReason;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 退款进度
     */
    private Integer refundStatus;

    /**
     * 退款失败原因，失败才填写
     */
    private String refundFailReason;

    /**
     * 添加时间
     */
    private Date rawAddTime;

    /**
     * 修改时间
     */
    private Date rawUpdateTime;


}
