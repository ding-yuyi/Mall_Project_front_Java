package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 收藏表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("collection")
public class Collection implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "conllection_id", type = IdType.AUTO)
    private Integer conllectionId;

    /**
     * 关联用户详情表id
     */
    private Integer userDetailId;

    /**
     * 商品id关联商品
     */
    private Integer skuId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 记录添加时间
     */
    private Date rawAddTime;

    /**
     * 记录更新时间
     */
    private Date rawUpdateTime;


}
