package com.example.jade_wine_front.entity;

import lombok.Data;

@Data
public class OrdersAndOrderDetail {
    private Integer orderId;
    private Integer userId;
    private String orderNum;
    private Integer orderState;
    private Integer addressId;
    private Integer orderTotal;
    private Integer ordersDetailId;
    private Integer skuId;
    private Integer skuNum;
    private Integer spuId;
    private String skuAttributeSpecs;
    private String skuKey;
    private Double spuPrice;
    private Integer spuStock;
    private Integer categoryId;
    private String spuName;
    private String skuName;
    private String spuAttributeList;
    private String spuOriginPlace;
    private String spuBrand;
    private String spuDescription;
    private Integer parentId;
    private String categoryName;
}
