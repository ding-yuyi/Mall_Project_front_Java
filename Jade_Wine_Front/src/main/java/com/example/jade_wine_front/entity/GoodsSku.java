package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * sku属性键表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("goods_sku")
public class GoodsSku implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "sku_id", type = IdType.AUTO)
    private Integer skuId;

    /**
     * 分类表主键
     */
    private Integer categoryId;

    /**
     * 商品表主键
     */
    private Integer spuId;

    /**
     * json字符串
     */
    private String skuAttributeSpecs;

    /**
     * 名字
     */
    private String skuName;

    /**
     * 图片地址
     */
    private String src;

    /**
     * 价格
     */
    private String spuPrice;

    /**
     * 选项名称
     */
    private String skuKey;

    /**
     * 商品总库存数量
     */
    private Long spuStock;
}
