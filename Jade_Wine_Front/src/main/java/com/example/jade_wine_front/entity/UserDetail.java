package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("user_detail")
public class UserDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "user_detail_id", type = IdType.AUTO)
    private Integer userDetailId;

    /**
     * 用户登录表Id(关联用户登录表)
     */
    private Integer userId;

    private String userHeadImage;

    /**
     * 用户性别（0：女，1男）
     */
    private Integer userDetailGender;

    /**
     * 用户出生日期
     */
    private Date userDetailBirthday;

    /**
     * 用户余额（默认为0）
     */
    private BigDecimal userDetailBalance;

    /**
     * 姓名(个人身份实名认证可以为空，但购买商品需要实名认证，防止未成年人购买)
     */
    private String userDetailName;

    /**
     * 实名认证身份证号
     */
    @TableField("user_detail_ID_number")
    private String userDetailIdNumber;

    /**
     * 个人简介
     */
    private String userDetailIntro;

    /**
     * 积分(默认为0)
     */
    private Long userDetailIntegral;

    /**
     * 昵称 不填则随机生成
     */
    private String nickname;
}
