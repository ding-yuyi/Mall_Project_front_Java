package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户地址表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("address")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "user_address_id", type = IdType.AUTO)
    private Integer userAddressId;

    /**
     * 用户地址
     */
    private String userAddressAddr;

    /**
     * 电话
     */
    private Long userAddressTel;

    private Integer userId;

    /**
     * 收货人姓名
     */
    private String userAddressName;

    /**
     * 默认收货地址（1：为默认地址 0：为普通地址），默认为0
     */
    private Integer userAddressDefaultAddr;

    /**
     * 记录添加时间
     */
    private Date rawAddTime;

    /**
     * 记录更新时间
     */
    private Date rawUpdateTime;


}
