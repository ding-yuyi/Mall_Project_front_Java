package com.example.jade_wine_front.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * spu商品表
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("goods_spu")
public class GoodsSpu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "spu_id", type = IdType.AUTO)
    private Integer spuId;

    /**
     * 分类id
     */
    private Integer categoryId;

    /**
     * 分类id
     */
    private String type;
    /**
     * 酒精度
     */
    private Integer alcoholContent;

    /**
     * 容量
     */
    private Integer capacity;

    /**
     * 商品名字
     */
    private String spuName;

    /**
     * json字符串，前端解析拼接
     */
    private String spuAttributeList;

    /**
     * 原产地
     */
    private String spuOriginPlace;

    /**
     * 品牌
     */
    private String spuBrand;


    /**
     *
     * 商品描述
     */
    private String spuDescription;

    /**
     * 记录添加时间
     */

    private Date rawAddTime;

    /**
     * 记录更新时间
     */
    private Date rawUpdateTime;


}
