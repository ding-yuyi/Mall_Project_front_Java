package com.example.jade_wine_front.controller;


import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.controller.form.AddOrdersForm;
import com.example.jade_wine_front.controller.form.FindOrderByNameForm;
import com.example.jade_wine_front.controller.form.PageQueryOrdersByIdForm;
import com.example.jade_wine_front.controller.form.UpdateOrderStatueByOrderIdForm;
import com.example.jade_wine_front.service.OrdersService;
import com.example.jade_wine_front.util.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@RestController
@Slf4j
public class OrdersController {
    /**
     * 需要用户的增删改查功能入口
     */
    @Resource
    private OrdersService ordersService;

    /**
     * 用户订单的分页查询
     * 参数:  pageNum 页码  pageSize 页容量    userId 用户id
     */
    @GetMapping("userOrders")
    public Result pageQueryOrdersByUserId(PageQueryOrdersByIdForm form) {
        log.info("根据userId分页查询orders{}", form);
        return ordersService.pageQueryOrdersById(form);
    }

    /**
     * 订单状态以及地址修改接口
     * 参数：   orderId 订单id   orderStatue 订单状态    addressId 地址id
     */
    @PutMapping("userAddressAndOrderState")
    public Result setOrderStatueAndAddressIdByOrderId(@RequestBody UpdateOrderStatueByOrderIdForm form) {
        log.info("用户修改地址以及修改订单状态功能{}", form);
        return ordersService.updateOrderStatueAndAddressByOrderId(form);
    }

    /**
     * 订单新增接口
     * 参数:   userId 用户id   orderNum 订单编号   orderState 订单状态     addressId   地址id
     */
    @PostMapping("order")
    public Result addOrders(AddOrdersForm form) {
        log.info("新增订单接口{}", form);
        return ordersService.addOrder(form);
    }

    /**
     * 删除订单接口
     * 参数: orderId 订单id
     */
    @DeleteMapping("order")
    public Result deleteOrdersByOrderId(Integer orderId) {
        log.info("删除订单接口{}", orderId);
        return ordersService.deleteOrderById(orderId);
    }

    /**
     * 查询订单以及订单详情接口
     * 参数：  userId 用户id     pageNum 页码      pageSize 页容量
     *
     * @param form
     * @return
     */
    @GetMapping("usersOrder")
    public Result queryOrdersAndOrderDetailByUserId(PageQueryOrdersByIdForm form, HttpServletRequest request) {
        log.info("用户订单以及订单详情查询入口{}", form);
        String auth_token = request.getHeader("auth_token");
        Claims parse = JwtUtils.parse(auth_token);
        Integer userId = (Integer) parse.get("userId");
        form.setUserId(userId);
        return ordersService.queryOrdersAndOrderDetailByUserId(form);
    }

    /**
     * 查询订单以及订单详情接口
     * 参数：  userId 用户id     pageNum 页码      pageSize 页容量   skuName 模糊查询商品名
     *
     * @param form
     * @return
     */
    @GetMapping("findOrderByName")
    public Result findOrderByName(FindOrderByNameForm form, HttpServletRequest request) {
        log.info("用户订单以及订单详情查询入口{}", form);
//        String auth_token = request.getHeader("auth_token");
//        Claims parse = JwtUtils.parse(auth_token);
//        Integer userId = (Integer) parse.get("userId");
        form.setUserId(9);
        return ordersService.findOrderByName(form);
    }
}

