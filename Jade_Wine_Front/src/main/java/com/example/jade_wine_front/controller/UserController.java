package com.example.jade_wine_front.controller;

import com.example.jade_wine_front.common.ControllerBase;
import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.controller.form.LoginForm;
import com.example.jade_wine_front.controller.form.RegisterForm;
import com.example.jade_wine_front.controller.form.UpdateUserDetailForm;
import com.example.jade_wine_front.entity.User;
import com.example.jade_wine_front.entity.UserDetail;
import com.example.jade_wine_front.service.UserService;
import com.example.jade_wine_front.util.EncryptUtil;
import com.example.jade_wine_front.util.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@RestController
@Slf4j
@RequestMapping("user")
public class UserController extends ControllerBase {

    @Resource
    private UserService userService;



    //查询邮箱是否已使用
    @GetMapping("email")
    public Result mailIsExists(String email) {
        log.info("开始查询邮箱：{}", email);
        Boolean isExits = userService.mailIsExits(email);
        if (isExits) {
            return Result.fail("邮箱已存在");
        }
        return Result.success("邮箱可用");
    }

//    //查询用户名是否可用
//    @GetMapping("username")
//    public Result usernameIsExists(String username){
//        Boolean isExits = userService.usernameIsExits(username);
//        if (isExits) {
//            return Result.fail("用户名已存在");
//        }
//        return Result.success("用户名可用");
//    }

    //发送验证码
    @PostMapping("verification")
    public Result sendVerificationCode(String email) throws MessagingException {
        log.info("进入controller,{}", email);
        String verificationCode = userService.sendVerificationCode(email);
        log.info(verificationCode);
        if (null == verificationCode) {
            return Result.fail("发送失败");
        }
        return Result.success("发送成功");
    }

    //校验验证码
    @GetMapping("verification")
    public Result checkVerification(String verification, String email) {
        log.info("邮箱：{},验证码：{}", email, verification);
        Boolean flag = userService.checkVerification(verification, email);
        if (flag) {
            return Result.success("验证码正确");
        }
        return Result.fail("验证码不正确");
    }

    //注册功能
    @PostMapping("user")
    public Result register(@RequestBody @Valid RegisterForm form, BindingResult result) throws InvocationTargetException, IllegalAccessException, MessagingException {
        //对密码加密
        form.setPassword(EncryptUtil.encrypt(form.getPassword()));
        //打印用户信息
        log.info("用户信息:{}", form);
        //参数校验
        Result validResult = extractError(result);
        if (validResult != null) {
            return validResult;
        }

        boolean flag = userService.register(form);
        if (flag) {
            return Result.success("注册成功");
        }

        return Result.fail("注册失败，请重新注册");
    }

    //登录功能
    @GetMapping("user")
    public Result login(HttpServletResponse response, @Valid LoginForm form, BindingResult result) {
        //打印用户信息
        log.info("用户信息:{}", form);
        //对密码加密
        form.setPassword(EncryptUtil.encrypt(form.getPassword()));

        Result validResult = extractError(result);
        if (validResult != null) {
            return validResult;
        }

        User user = userService.login(form);
        log.info("返回结果，{}", user);
        if (null == user) {
            return Result.fail("登录失败");
        }
        String jwt = JwtUtils.createJWT(user);
        log.info("jwt：{}", jwt);
        response.setHeader(JwtUtils.AUTH_TOKEN_NAME, jwt);
        HashMap<String, Object> userInfo = new HashMap<>();
        userInfo.put("auth_token", jwt);
        userInfo.put("loginUser", user);
        return Result.success(userInfo);

    }

    //修改密码
    @PutMapping("pwd")
    public Result updatePassword(HttpServletRequest request, String oldPassword, String newPassword) throws InvocationTargetException, IllegalAccessException {
        //先对密码加密
        oldPassword = EncryptUtil.encrypt(oldPassword);
        newPassword = EncryptUtil.encrypt(newPassword);
        log.info("旧密码,{},新密码,{}", oldPassword, newPassword);

        //解析token
        String token = request.getHeader("auth_token");
        Claims parse = JwtUtils.parse(token);
        Integer userId = (Integer) parse.get("userId");

        Boolean flag = userService.updatePwd(userId, oldPassword, newPassword);
        if (flag) {
            return Result.success("修改成功");
        }
        return Result.success("修改失败");
    }

    //查询个人信息
    @GetMapping("userDetail")
    public Result getUserDetail(HttpServletRequest request) {
        //解析token
        String token = request.getHeader("auth_token");
        Claims parse = JwtUtils.parse(token);
        String userId = (String) parse.get("user_id");
        log.info("查询个人信息:{}", userId);
        UserDetail userDetail = userService.getUserDetail(Integer.valueOf(userId));
        if (null == userDetail) {
            return Result.fail("查询失败");
        }
        return Result.success(userDetail);
    }

    //修改邮箱
    @PutMapping("email")
    public Result updateEmail(String oldEmail, String newEmail, String verification) {
        log.info("原邮箱:{},新邮箱,{},验证码,{}", oldEmail, newEmail, verification);

        String msg = userService.updateEmail(oldEmail, newEmail, verification);

        if (msg.equals("修改成功")) {
            return Result.success(msg);
        }
        return Result.fail(msg);
    }

    //修改个人信息
    @PutMapping("userDetail")
    public Result updateUserDetail(HttpServletRequest request, @RequestBody UpdateUserDetailForm form) {
        log.info("修改的信息:{}", form);
        //解析token
        String token = request.getHeader("auth_token");
        Claims parse = JwtUtils.parse(token);
        Integer userId = (Integer) parse.get("userId");

        Boolean flag = userService.updateUserDetail(form, userId);
        if (flag) {
            return Result.success("修改成功");
        }
        return Result.success("修改失败");
    }

    //上传头像
    @PostMapping("headImg")
    public Result uploadHeadImage(MultipartFile file) throws IOException {
        Boolean flag = userService.uploadHeadImage(file);
        return Result.success("上传成功");
    }

    //绑定手机
    @PostMapping("tel")
    public Result addTel(HttpServletRequest request, @RequestBody String tel) {
        log.info("用户手机:{}", tel);

        //解析token
        String token = request.getHeader("auth_token");
        Claims parse = JwtUtils.parse(token);
        Integer userId = (Integer) parse.get("userId");
        System.out.println("userId:" + userId);

        int index = tel.indexOf("=");
        String substring = tel.substring(index + 1);

        Boolean flag = userService.addTel(userId, substring);
        if (flag) {
            return Result.success("添加成功");
        }
        return Result.success("添加失败");
    }

    //注销用户
    @DeleteMapping("user")
    public Result deleteUser(HttpServletRequest request) {
        log.info("进入注销操作");
        //解析用户
        String token = request.getHeader("auth_token");
        Claims parse = JwtUtils.parse(token);
        String userId = (String) parse.get("user_id");

        Boolean flag = userService.deleteUser(Integer.valueOf(userId));

        if (flag) {
            return Result.success("注销成功");
        }
        return Result.success("注销失败");
    }
}

