package com.example.jade_wine_front.controller.form;

import lombok.Data;

/**
 * @PackageName:
 * @ClassName: FindByCategoryForm
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/14 15:02
 */
@Data
public class FindByCategoryForm {
    private Integer categoryId;
    private Integer pageIdx;
    private Integer pageSize;
}
