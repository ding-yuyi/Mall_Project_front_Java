package com.example.jade_wine_front.controller;

import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.entity.Address;
import com.example.jade_wine_front.service.AddressService;
import com.example.jade_wine_front.util.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Title: AddressController
 * @ProjectName Jade_Wine_Front
 * @Description: 地址相关操作的控制层
 * @Author kevinkachi
 * @Date 2021/7/10 20:48
 */
@RestController
@RequestMapping("address")
@Slf4j
public class AddressController {
    @Resource
    private AddressService addressService;

    //根据用户id查询所选地址的方法
    @GetMapping("addressByUser")
    public Result addressByUser(HttpServletRequest request) {
        String auth_token = request.getHeader("auth_token");
        Claims parse = JwtUtils.parse(auth_token);
        Integer userId = (Integer) parse.get("userId");
        System.out.println(userId + "userId");
        List<Address> addresses = addressService.addressByUser(userId);
        return Result.success(addresses);
    }
}
