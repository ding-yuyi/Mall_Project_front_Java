package com.example.jade_wine_front.controller.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @PackageName:
 * @ClassName: RegisterForm
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/9 16:36
 */
@Data
public class RegisterForm {
    @NotBlank
    @Length(min = 6, max = 20)
    private String password;
    @NotBlank
    @Pattern(regexp = "(\\w{3,10}\\.)*\\w+@\\w+(\\.\\w{2,3})*\\.\\w{2,3}", message = "邮箱不合法")
    private String email;
}
