package com.example.jade_wine_front.controller.form;

import lombok.Data;

@Data
public class UpdateOrderStatueByOrderIdForm {
    private Integer orderId;
    private Integer orderState;
    private Integer addressId;
}
