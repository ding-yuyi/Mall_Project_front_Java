package com.example.jade_wine_front.controller.form;

import lombok.Data;

@Data
public class FindOrderByNameForm {
    private Integer pageNum;
    private Integer pageSize;
    private Integer userId;
    private String skuName;
}
