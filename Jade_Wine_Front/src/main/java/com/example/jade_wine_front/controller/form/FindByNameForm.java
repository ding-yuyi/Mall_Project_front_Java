package com.example.jade_wine_front.controller.form;

import lombok.Data;

/**
 * @PackageName:
 * @ClassName: FindByNameForm
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/15 14:24
 */
@Data
public class FindByNameForm {
    private Integer pageNum;
    private Integer pageSize;
    private String skuName;
}
