package com.example.jade_wine_front.controller.form;

import lombok.Data;

import java.util.Date;

/**
 * @PackageName:
 * @ClassName: UpdateUserForm
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/10 15:09
 */
@Data
public class UpdateUserForm {
    private Integer userDetailGender;

    private Date userDetailBirthday;

    private String tel;

    private String email;

    private String password;

    private String userDetailIntro;
}
