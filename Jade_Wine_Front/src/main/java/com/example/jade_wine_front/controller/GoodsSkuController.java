package com.example.jade_wine_front.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.controller.form.*;
import com.example.jade_wine_front.entity.GoodsSku;
import com.example.jade_wine_front.entity.GoodsSpu;
import com.example.jade_wine_front.service.GoodsSkuService;
import com.example.jade_wine_front.service.GoodsSpuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * sku属性键表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@RestController
@Slf4j
@RequestMapping("goods")
public class GoodsSkuController {

    @Resource
    private GoodsSpuService goodsSpuService;

    @Resource
    private GoodsSkuService goodsSkuService;

    //分类查询
    @GetMapping("category")
    public Result findByCategory(FindByCategoryForm form) {
        log.info("分类id,{}", form);
        Page skuPage = goodsSkuService.findByCategory(form);

        if (null == skuPage.getRecords()) {
            return Result.fail("网络正忙，查询失败");
        }

        return Result.success(skuPage);
    }

    /**
     * 根据商品ID分查询
     *
     * @return
     */
    @GetMapping("one")
    public Result findGoodsSpu(int spuId) {
        log.info("根据商品ID进行唯一查询{}", spuId);
        GoodsSpu goodsSpu = goodsSpuService.findGoodsSpu(spuId);
        return Result.success(goodsSpu);
    }//l


    /**
     * 查询所有的商品(搜索框)
     *
     * @return
     */
    @GetMapping("all")
    public Result queryGoodsSpuByAll(PageQueryGoodsSpuByIdForm form) {
        log.info("查询所有的商品{}", form);
        return goodsSpuService.queryGoodsSpuByAll(form);
    }

    //根据名字查询商品

    /**
     * 代码修改：搜索框业务
     * 需要进行模糊查询，并进行分页
     *
     * @param
     * @return
     */
    @GetMapping("/findByName")
    public Result queryGoodsfindByName(FindByNameForm form) {
        log.info("根据名字查询商品{}", form);
        Page<GoodsSku> goodsSkuPage = goodsSkuService.queryGoodsfindByName(form);
        if (null == goodsSkuPage.getRecords()) {
            return Result.fail("查询失败");
        }
        return Result.success(goodsSkuPage);
    }


    //根据类别查询

    /**
     * 修改：分类查询，结果多个，需要进行分页查询
     *
     * @param
     * @return
     */
    @GetMapping("findCount")
    public Result queryGoodsfindByCount(FindCountForm form) {
        log.info("根据类别查询{},", form);
        ReturnFindCountForm returnFindCountForm = goodsSkuService.findCount(form);


        log.info("查询的商品,{}", returnFindCountForm);
        if (null == returnFindCountForm) {
            return Result.fail("查询失败");
        }

        return Result.success(returnFindCountForm);
    }


    /**
     * 通过id查询商品详情,进入详情页查询商品
     */
    @GetMapping("skuids")
    public Result goodsSkuById(int skuId) {
        log.info("根据id查询商品详情");
        GoodsSku goodsSku = goodsSkuService.goodsSkuById(skuId);
        return Result.success(goodsSku);
    }


    /**
     * 联合查询3张表goods_sku goods_spu category
     */

    @GetMapping("uniteId")
    public Result goodsSkuSpu(Integer skuId) {
        log.info("查询3张表的数据:{}", skuId);
        GoodsSkuSpu goodsSkuSpu = goodsSkuService.goodsSkuSpu(skuId);
        return Result.success(goodsSkuSpu);
    }
}

