package com.example.jade_wine_front.controller.form;

import lombok.Data;

@Data
public class PageQueryOrdersByIdForm {
    private Integer pageNum;
    private Integer pageSize;
    private Integer userId;
}
