package com.example.jade_wine_front.controller.form;

import lombok.Data;

/**
 * @Title: NewCartInfoForm
 * @ProjectName Jade_Wine_Front
 * @Description: TODO
 * @Author kevin
 * @Date 2021/7/10 17:54
 * @Version: 1.0
 */
@Data
public class NewCartInfoForm {
    //用户id
    private Integer userId;

    //商品id
    private Integer skuId;

    //商品数量
    private Integer skuNum;


}
