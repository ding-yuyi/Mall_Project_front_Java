package com.example.jade_wine_front.controller.form;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
public class GoodsSkuSpu {
    /**
     * 联合查询中的商品名字
     */
    private String skuId;
    /**
     * 联合查询中的商品名字
     */
    private String skuName;
    /**
     * 联合查询中的酒精度
     */
    private String categoryName;
    /**
     * 联合中的商品价格
     */
    private BigDecimal spuPrice;
    /**
     * 联合查询中的原产地
     */
    private String spuOriginPlace;

    /**
     * 联合查询中的品牌
     */
    private String spuBrand;

    /**
     * 联合查询中的商品描述
     */
    private String spuDescription;

    /**
     * 联合查询中的json字符串(属性)
     */
    private String skuAttributeSpecs;

    /**
     * 联合查询中的商品图片地址
     */
    private String src;

    /**
     * 联合查询中的类型
     */
    private String type;

    /**
     * 联合查询中的酒精度
     */
    private String alcoholContent;


    /**
     * 容量
     */
    /**
     * 联合查询中的库存
     */
    private BigInteger spuStock;
}
