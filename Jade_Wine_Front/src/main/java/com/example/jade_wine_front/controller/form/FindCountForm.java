package com.example.jade_wine_front.controller.form;

import lombok.Data;

/**
 * @PackageName:
 * @ClassName: FindCountForm
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/15 17:42
 */
@Data
public class FindCountForm {
    private Integer pageIdx;
    private Integer pageSize;
    private Integer categoryId;
    private String capacity;
    private String alcoholContent;
    private String type;
    private String spuOriginPlace;
}
