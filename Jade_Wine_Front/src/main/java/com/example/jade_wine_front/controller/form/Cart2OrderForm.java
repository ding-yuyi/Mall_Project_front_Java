package com.example.jade_wine_front.controller.form;

import com.example.jade_wine_front.entity.Cart;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Title: Cart2OrderForm
 * @ProjectName Jade_Wine_Front
 * @Description: 把选择的购物车信息传到后端用来接收的form
 * @Author kevin
 * @Date 2021/7/10 11:35
 * @Version: 1.0
 */
@Data
public class Cart2OrderForm {
    //购物车信息集合
    private List<Cart> cart;

    //用户选定的地址条目
    private Integer userAddressId;

    //前端算出来的订单总价
    private BigDecimal orderPrice;
}
