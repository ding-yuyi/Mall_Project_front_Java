package com.example.jade_wine_front.controller.form;

import lombok.Data;

import java.util.Date;

/**
 * @PackageName:
 * @ClassName: UpdateUserDetailForm
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/12 15:31
 */
@Data
public class UpdateUserDetailForm {
    /**
     * 用户性别（0：女，1男）
     */
    private Integer userDetailGender;

    /**
     * 用户出生日期
     */
    private Date userDetailBirthday;

    /**
     * 个人简介
     */
    private String userDetailIntro;

    /**
     * 昵称 不填则随机生成
     */
    private String nickname;
}
