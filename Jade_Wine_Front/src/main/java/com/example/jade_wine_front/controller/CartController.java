package com.example.jade_wine_front.controller;

import com.example.jade_wine_front.common.Result;
import com.example.jade_wine_front.controller.form.Cart2OrderForm;
import com.example.jade_wine_front.controller.form.CartProductNumForm;
import com.example.jade_wine_front.controller.form.NewCartInfoForm;
import com.example.jade_wine_front.entity.Cart;
import com.example.jade_wine_front.service.CartService;
import com.example.jade_wine_front.util.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 购物车表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-09
 */
@CrossOrigin("*")
@RestController
@RequestMapping("cart")
@Slf4j
public class CartController {

    @Resource
    private CartService cartService;


    /*
     * @Author kevinkachi
     * @Description 根据用户id查询所属购物车信息的方法
     * @Date  2021/7/9
     * @Param [request]
     * @Return com.example.jade_wine_front.common.Result
     * @MethodName cartByUser
     */
    @GetMapping("cartByUser")
    public Result cartByUser(HttpServletRequest request) {
        log.info("controller，查询购物车");
//        解析token，获取其中的用户id
        String token = request.getHeader("auth_token");
        Claims parse = JwtUtils.parse(token);
        Integer userId = (Integer) parse.get("userId");
        List<Cart> carts = cartService.cartByUser(userId);
        log.info("购物车:{}", carts);
        return Result.success(carts);
    }

    /*
     * @Author kevin
     * @Description 根据购物车id修改对应条目的商品数量，数量为前端手动输入
     * @Date  2021/7/10
     * @Param [form]
     * @Return com.example.jade_wine_front.common.Result
     * @MethodName cartProductNum
     */
    @PutMapping("cartProductNum")
    public Result cartProductNum(@RequestBody CartProductNumForm form) {
        log.info("controller，修改购物车商品数量{}", form);
        Cart cart = new Cart();
        BeanUtils.copyProperties(form, cart);
        Integer integer = cartService.cartProductNum(cart);
        if (integer == 1) {
            return Result.success();
        }
        return Result.fail();
    }

    /*
     * @Author kevin
     * @Description 删除一条购物车信息的方法
     * @Date  2021/7/10
     * @Param [cartId]
     * @Return com.example.jade_wine_front.common.Result
     * @MethodName cartInfo
     */
    @DeleteMapping("cartInfo")
    public Result cartInfo(String cartId) {
        log.info("controller，删除购物车{}", cartId);
        Integer integer = cartService.cartInfo(Integer.valueOf(cartId));
        if (integer == 1) {
            return Result.success();
        }
        return Result.fail();
    }

    /*
     * @Author kevin
     * @Description 订单确认页把信息存入订单表和订单详情表的方法
     * @Date  2021/7/10
     * @Param [form]
     * @Return com.example.jade_wine_front.common.Result
     * @MethodName cart2Order
     */
    @PostMapping("cart2Order")
    public Result cart2Order(@RequestBody Cart2OrderForm form) {
        log.info("controller，购物车转换订单{}", form);
        List<Cart> carts = form.getCart();
        BigDecimal orderPrice = form.getOrderPrice();
        Integer userAddressId = form.getUserAddressId();
        return cartService.cart2Order(carts, orderPrice, userAddressId);
//        return null;
    }

    @PostMapping("newCartInfo")
    public Result newCartInfo(@RequestBody NewCartInfoForm form, HttpServletRequest request) {
        log.info("controller，新增购物车条目{}", form);
        if (null == form.getSkuNum()) {
            form.setSkuNum(1);
        }
        //获取token中携带的用户id
        String auth_token = request.getHeader("auth_token");
        Claims parse = JwtUtils.parse(auth_token);
        Integer userId = (Integer) parse.get("userId");
        //相应信息存入cart类
        Cart cart = new Cart();
        BeanUtils.copyProperties(form, cart);
        cart.setUserId(Integer.valueOf(userId));
        log.info("购物车,{}", cart);
        Integer integer = cartService.newCartInfo(cart);
        if (integer == 1) {
            return Result.success();
        }
        return Result.fail();
    }
}

