package com.example.jade_wine_front.controller.form;

import com.example.jade_wine_front.entity.GoodsSku;
import lombok.Data;

import java.util.List;

/**
 * @PackageName:
 * @ClassName: ReturnFindCountForm
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/15 17:48
 */
@Data
public class ReturnFindCountForm {

    private List<GoodsSku> goodsSkus;

    private Long current;
    private Long size;
    private Long total;
}
