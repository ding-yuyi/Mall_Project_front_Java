package com.example.jade_wine_front.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.example.jade_wine_front.config.AlipayConfig;
import com.example.jade_wine_front.controller.form.PayItemInformationParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Title: PayServlet
 * @ProjectName woniumall
 * @Description: TODO
 * @Author kevin
 * @Date 2021/5/21 17:30
 * @Version: 1.0
 */
@RestController
@RequestMapping("api")
@Slf4j
public class PayController {

    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    @ResponseBody
    public void ailpay(@RequestBody PayItemInformationParam payItemInformationParam, HttpServletResponse response) {

        log.info("alipay");
        //去沙箱里面找自己的
        AlipayClient alipayClient = new
                DefaultAlipayClient("https://openapi.alipaydev.com/gateway.do",
                "2021000117661878",
                "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCP0S9OFBIC6elNK6Lo2b2UouCLKq1qeGeo78Y+zw+SDx8DU+BG5PUgyZtd1r46cZeqw4lMCxNmw0rLr4kKnTFNkI4s80a+K1zOEvGjBD+istrfZkwSRDohVXBg4vOW7HmZu3tuG007G2OEfv1nKz8Kem1LKGBY79RC5UBKPlR6H74jVHXrkv4y/rNPe+ksWj42UYboxfb066r8F2+LqinF9hTujYlJ9fL+WkXImDhIEsR0xn3SA+x4OQzpqw6oYKxSi+zpPr+1pQpFlaDzm7fOI/lkKVSDjhhNJKiHH6aPOPSXZWVBFAdbkd1ttfj6A2OpT+O/2KmhKwW4ZHxIEuyfAgMBAAECggEAZXgCt0Tj3Fhq+b6i50TRRHCSmr3WsrsW8BOUPLZc1wGCMRfZsktSQ/raep/LgXKpvprNYPCjlKqPOBy2RN3Zbk9icIaIUB5KET8+a3TQ7Rrh4CLN40lt9hzQ+BrIIKDhvryPfoUWvXA6i74JTeQuS1dLi26GY9l6WmRjMr69XZe9gzIqe3LOeMVrYbbHRlpQfyr5IUW6d/A4VU7vML+zwLLKl3NTeBh686of3ZjA3+fY8eaKfZOfN6yFXMv09HZB4imR7qmc/YIZsn7SPwbbcXyL3C1IoTJGak9nsa4j5pg74gB/P268znIn6HwBn9jL7/iq/MPS+SNsCsugDQsSQQKBgQDVeOzCmGDn7PqglA9hvtlot3OTbbt2v7GyshTIe25yeAUTO5BeKlzwjr81O8Z3pRs3d7YN0e+u69Gb/4leH+7G41a6icj0QND42WQfTYy1k8Tx3QjAh3zBHY9rvEkHzxPjPpuEAJR8mw3nAWbfPvEr540PZcgT2mu/vDnfwxV5IQKBgQCsd9kVURiCUGzPmKu2CGpbV5gmwgTk0XNCc9Y22jQg2WKRmIOa4qzDaH8aRgyKnSCo41sT6W8xw+clCSaL4ohqpnKwirPMpWHwQYg7GTdcfg9XYbHLc6Irt0hKteOr/scmeP31vLmmughSEekHpkYfjBkxXgp/FShhxXItKqrtvwKBgB3/DP3XHZ7ZXOpdg7/PaotdY98uhy4Lh7O9dFArj/yDwurN7t5cg4zF8jZPvPao+6cSqkFM46ontt99y9avFAVcgp5ZqCQyS2r8WSZQ5lnJdt4pmgY7w5r7RWD1Jynyvi+rZ3zn+/V6cDyqpMLv/EGYGukz+yZXBdb+Yv9/UTAhAoGAV/2dR37kXjfiC9f8YxbQDGIYQk3iaUti3phyxfF1fvzpiRairjbPpbJ+hk0OwPmBXCkCopoKHP2xw/dzLxMYPjFFekSJjvMhkI4ejuhCknHOKeqx5vbL2jncLCG4UlveBwbbqq/ql+F5tUVl+n0ecaHsiH9OI6ALxjYSFnEaW8kCgYEAr/iwQrFOvkkH8sqY9RKi65wGFZ8TjpIACaxk5t8zo25C4AJKr0vh1NGQXxdF56ptjaLqw2p5bxNm3rduGWuGo/2Kc7XsuGkJuogbh7mosKwzoiWOQ8KOXjOKDU8hHEyhNqq8zKTvE3oepC0IPY3VR/re+6xivuf1hDNjCtLzYaQ=",  //私钥 不知道是什么 往上面看 配置沙箱密钥的时候 自己保存的
                "json", "utf-8",
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgLm367UkOwrD7WXe0Yu71Ctj8HScvnz7sJMxxBALRyDSw3Dvsd0B44Sl0pav3t2AIG71XKoAYf1HIrOqZeNWxonW/jUutLtRsckaxpq3XLcKURQaKozXDdMMJdFmQHUbItJoQLb21k4sw7u1bXV6xcUPEM6fkMAEmnRRazXagmcqxeI2TNnxaaFCheMCfk684GuMPLZZokXIjNMl9pfpcMeunfGMjxxH3yMfKmJeZLzzh+ApVOKgPOl5CsMegKaksMeDxWOuF+EsjqqWioC0ni02gkV6OobT1P8uZ+krqF8/TSKHaGKf6116nLetYixXcK540fRqaA6ykwGaGHuoTwIDAQAB", "RSA2");

        //订单号  自定义
        String out_trade_no = payItemInformationParam.getOutTradeNo();
//        out_trade_no = "12345";
        String total_amount = payItemInformationParam.getTotalAmount();
//        total_amount = ;
        String subject = payItemInformationParam.getSubject();
//        subject = "test";
        String body = "描述";
        body = "body";
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//创建API对应的request
        //同步通知
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        //异步通知
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

        //配置参数
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\","
                + "\"body\":\"" + body + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String form = "";
//        try {
//            form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
//        } catch (AlipayApiException e) {
//            e.printStackTrace();
//        }
//        //form就是一个表单 html 直接给前端 替换 body标签里面的东西
//        return form;
        try {
            form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
            System.out.println("生成表单");
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        response.setContentType("text/html;charset=" + AlipayConfig.charset);
        AlipayConfig.logResult(form);// 记录支付日志
        try {
            response.getWriter().write(form);//直接将完整的表单html输出到页面
            response.getWriter().flush();
            response.getWriter().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    @RequestMapping(value = "/pay", method = RequestMethod.POST)
//    @ResponseBody
//    public void pay(@RequestBody PayItemInformationParam payItemInformationParam, HttpServletResponse response) {
//        //开放平台 SDK 封装了签名实现，只需在创建 DefaultAlipayClient 对象时，下面是设置顺序
//        //1.设置请求网关 (gateway)，
//        //2.应用 id (app_id)，
//        //3,应用私钥 (private_key)，
//        //4.返回格式 (format)，
//        //5.编码格式 (charset)，
//        //6.支付宝公钥 (alipay_public_key)，
//        //7.签名类型 (sign_type)
//        //根据你的AlipayConfig里面的变量名来设置对应的值,获取AlipayConfig里面初始值
//        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.URL, AlipayConfig.APPID, AlipayConfig.RSA_PRIVATE_KEY, AlipayConfig.FORMAT, AlipayConfig.CHARSET, AlipayConfig.ALIPAY_PUBLIC_KEY, AlipayConfig.SIGNTYPE);
//        // 创建API对应的request
//        AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();
//        // 在公共参数中设置同步回跳和异步通知地址
//        System.out.println("设置回调地址");
//        alipayRequest.setReturnUrl(AlipayConfig.return_url);
//        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);
//        //将前端获取到的值放到这个里面中,商品订单号,付款金额,订单名称，销售产品码是必填项，不能为空
//    /*alipayRequest.setBizContent("{\"out_trade_no\":\"201934369242223\","
//            + "\"total_amount\":\"88.88\","
//            + "\"subject\":\"小米手机\","
//            + "\"product_code\":\"QUICK_WAP_PAY\"}");*/
//        alipayRequest.setBizContent("{" +
//                " \"out_trade_no\":\"" + payItemInformationParam.getOutTradeNo() + "\"," +
//                " \"total_amount\":\"" + payItemInformationParam.getTotalAmount() + "\"," +
//                " \"subject\":\"" + payItemInformationParam.getSubject() + "\"," +
//                " \"product_code\":\"" + payItemInformationParam.getProductCode() + "\"" +
//                " }");//填充业务参数
//        String form = "";
//        try {
//            form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
//            System.out.println("生成表单");
//        } catch (AlipayApiException e) {
//            e.printStackTrace();
//        }
//        response.setContentType("text/html;charset=" + AlipayConfig.CHARSET);
//        AlipayConfig.logResult(form);// 记录支付日志
//        try {
//            response.getWriter().write(form);//直接将完整的表单html输出到页面
//            response.getWriter().flush();
//            response.getWriter().close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}