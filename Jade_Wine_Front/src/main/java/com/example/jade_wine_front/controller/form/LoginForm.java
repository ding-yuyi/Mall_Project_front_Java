package com.example.jade_wine_front.controller.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @PackageName:
 * @ClassName: LoginForm
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/10 9:39
 */
@Data
public class LoginForm {
    @NotBlank
    @Length(min = 2, max = 20)
    private String username;
    @NotBlank
    @Length(min = 6, max = 20)
    private String password;
}
