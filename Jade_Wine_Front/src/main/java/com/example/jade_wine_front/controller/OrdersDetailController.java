package com.example.jade_wine_front.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单详情表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-12
 */
@RestController
@RequestMapping("/jade_wine_front/orders-detail")
public class OrdersDetailController {

}

