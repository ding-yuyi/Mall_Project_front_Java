package com.example.jade_wine_front.controller.form;

import lombok.Data;

@Data
public class AddOrdersForm {
    private Integer userId;
    private String orderNum;
    private Integer orderState;
    private Integer addressId;
}
