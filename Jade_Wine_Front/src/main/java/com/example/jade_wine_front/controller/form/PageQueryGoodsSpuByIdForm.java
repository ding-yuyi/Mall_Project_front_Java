package com.example.jade_wine_front.controller.form;

import lombok.Data;

@Data
public class PageQueryGoodsSpuByIdForm {
    private Integer pageNum;
    private Integer pageSize;
    private Integer spu_name;
}
