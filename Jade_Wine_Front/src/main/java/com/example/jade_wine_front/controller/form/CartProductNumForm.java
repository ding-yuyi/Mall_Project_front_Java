package com.example.jade_wine_front.controller.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Title: CartProductNumForm
 * @ProjectName Jade_Wine_Front
 * @Description: 购物车修改数量的form
 * @Author kevin
 * @Date 2021/7/10 10:50
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartProductNumForm implements Serializable {
    private Integer cartId;

    private Integer skuNum;
}
