/*
Navicat MySQL Data Transfer

Source Server         : aliyun
Source Server Version : 50734
Source Host           : 39.105.5.134:3306
Source Database       : jade_wine

Target Server Type    : MYSQL
Target Server Version : 50734
File Encoding         : 65001

Date: 2021-07-10 21:07:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `user_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_address_addr` varchar(255) NOT NULL COMMENT '用户地址',
  `user_address_tel` bigint(20) NOT NULL COMMENT '电话',
  `user_address_name` varchar(128) NOT NULL COMMENT '收货人姓名',
  `user_address_default_addr` int(11) NOT NULL COMMENT '默认收货地址（1：为默认地址 0：为普通地址），默认为0',
  `user_id` int(11) NOT NULL COMMENT '''user表主键''',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`user_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户地址表';

-- ----------------------------
-- Records of address
-- ----------------------------

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `sku_id` int(11) NOT NULL COMMENT '商品id',
  `sku_num` int(11) NOT NULL COMMENT '商品数量',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录修改时间',
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='购物车表';

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('1', '1', '1', '8', '2021-07-09 07:21:55', '2021-07-10 17:12:52');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0' COMMENT '父级分类',
  `category_name` varchar(20) NOT NULL COMMENT '分类名称',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录修改时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='类别表';

-- ----------------------------
-- Records of category
-- ----------------------------

-- ----------------------------
-- Table structure for collection
-- ----------------------------
DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection` (
  `conllection_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_detail_id` int(11) NOT NULL COMMENT '关联用户详情表id',
  `sku_id` int(11) NOT NULL COMMENT '商品id关联商品',
  `content` varchar(500) NOT NULL COMMENT '评论内容',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`conllection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收藏表';

-- ----------------------------
-- Records of collection
-- ----------------------------

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `spu_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '分类id',
  `spu_name` varchar(64) NOT NULL COMMENT '商品名字',
  `spu_attribute_list` varchar(255) NOT NULL COMMENT 'json字符串，前端解析拼接',
  `spu_origin_place` varchar(20) NOT NULL COMMENT '原产地',
  `spu_brand` varchar(64) NOT NULL COMMENT '品牌',
  `spu_description` varchar(255) NOT NULL COMMENT '商品描述',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`spu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='spu商品表';

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku`;
CREATE TABLE `goods_sku` (
  `sku_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '分类表主键',
  `spu_id` int(11) NOT NULL COMMENT '商品表主键',
  `sku_attribute_specs` varchar(255) NOT NULL COMMENT 'json字符串',
  `sku_key` varchar(255) NOT NULL COMMENT '选项名称',
  `spu_stock` bigint(20) NOT NULL COMMENT '商品总库存数量',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`sku_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sku属性键表';

-- ----------------------------
-- Records of goods_sku
-- ----------------------------

-- ----------------------------
-- Table structure for goods_spu
-- ----------------------------
DROP TABLE IF EXISTS `goods_spu`;
CREATE TABLE `goods_spu` (
  `spu_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '分类id',
  `spu_name` varchar(64) NOT NULL COMMENT '商品名字',
  `spu_attribute_list` varchar(255) NOT NULL COMMENT 'json字符串，前端解析拼接',
  `spu_origin_place` varchar(20) NOT NULL COMMENT '原产地',
  `spu_brand` varchar(64) NOT NULL COMMENT '品牌',
  `spu_description` varchar(255) NOT NULL COMMENT '商品描述',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`spu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='spu商品表';

-- ----------------------------
-- Records of goods_spu
-- ----------------------------
INSERT INTO `goods_spu` VALUES ('1', '1', '红酒', '圣米亚赤霞珠干红葡萄酒', '法国庄园', '圣米亚', '圣米亚：容量750ML：酒精度：13%', '2021-07-09 09:24:02', '2021-07-10 12:10:55');
INSERT INTO `goods_spu` VALUES ('2', '2', '白酒', '', '', '', '', '2021-07-10 11:49:50', '2021-07-10 12:11:15');
INSERT INTO `goods_spu` VALUES ('3', '3', '洋酒', '', '', '', '', '2021-07-10 11:53:33', '2021-07-10 12:11:14');
INSERT INTO `goods_spu` VALUES ('4', '4', '啤酒', '', '', '', '', '2021-07-10 11:59:20', '2021-07-10 12:11:13');

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_src` varchar(255) NOT NULL COMMENT '图片路径',
  `spu_id` int(11) NOT NULL COMMENT '订单id',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录修改时间',
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片表';

-- ----------------------------
-- Records of image
-- ----------------------------

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `order_num` varchar(64) NOT NULL COMMENT '订单编号',
  `order_state` int(11) DEFAULT '0' COMMENT '订单状态',
  `address_id` int(11) NOT NULL COMMENT '地址id',
  `order_total` decimal(10,2) NOT NULL COMMENT '总金额',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间（新增时间）',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='订单表';

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1', '1', '13', '3', '2', '123123.00', '2021-07-22 10:06:55', '2021-07-10 02:10:00');

-- ----------------------------
-- Table structure for orders_detail
-- ----------------------------
DROP TABLE IF EXISTS `orders_detail`;
CREATE TABLE `orders_detail` (
  `orders_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `sku_id` int(11) NOT NULL COMMENT 'sku表主键',
  `sku_num` int(11) NOT NULL COMMENT '商品数量',
  `order_id` int(11) NOT NULL COMMENT '订单表id',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`orders_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单详情表';

-- ----------------------------
-- Records of orders_detail
-- ----------------------------

-- ----------------------------
-- Table structure for refund
-- ----------------------------
DROP TABLE IF EXISTS `refund`;
CREATE TABLE `refund` (
  `refund_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `refund_reason` varchar(255) NOT NULL COMMENT '退款原因',
  `order_id` int(11) NOT NULL COMMENT '订单id',
  `refund_status` int(11) DEFAULT '0' COMMENT '退款进度',
  `refund_fail_reason` varchar(255) DEFAULT NULL COMMENT '退款失败原因，失败才填写',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`refund_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退款表';

-- ----------------------------
-- Records of refund
-- ----------------------------

-- ----------------------------
-- Table structure for sku_key
-- ----------------------------
DROP TABLE IF EXISTS `sku_key`;
CREATE TABLE `sku_key` (
  `sku_key_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '分类id',
  `sku_key` varchar(20) NOT NULL COMMENT '选项名称',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`sku_key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sku属性键表';

-- ----------------------------
-- Records of sku_key
-- ----------------------------

-- ----------------------------
-- Table structure for sku_value
-- ----------------------------
DROP TABLE IF EXISTS `sku_value`;
CREATE TABLE `sku_value` (
  `sku_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `sku_key_id` int(11) NOT NULL COMMENT '选项id',
  `sku_value` varchar(30) NOT NULL COMMENT '具体属性',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`sku_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sku属性值表';

-- ----------------------------
-- Records of sku_value
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(20) NOT NULL COMMENT '密码',
  `user_status` int(11) NOT NULL COMMENT '用户状态是否激活 | 0 未激活 | 1已激活',
  `email` varchar(20) NOT NULL COMMENT '邮箱',
  `tel` bigint(20) NOT NULL COMMENT '电话',
  `active_code` varchar(64) NOT NULL COMMENT '激活码',
  `role` int(11) NOT NULL DEFAULT '0' COMMENT '角色 | 0 普通用户 | 1 管理员',
  `user_nickname` varchar(64) NOT NULL COMMENT '昵称 不填则随机生成',
  `user_last_login_time` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录修改时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for user_detail
-- ----------------------------
DROP TABLE IF EXISTS `user_detail`;
CREATE TABLE `user_detail` (
  `user_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_head_image` varchar(255) NOT NULL DEFAULT 'https://img2.baidu.com/it/u=2561659095,299912888&fm=26&fmt=auto&gp=0.jpg' COMMENT '用户头像',
  `user_id` int(11) NOT NULL COMMENT '用户登录表Id(关联用户登录表)',
  `user_detail_gender` int(11) NOT NULL DEFAULT '2' COMMENT '用户性别（0：女，1男，2 未填写）',
  `user_detail_birthday` datetime DEFAULT NULL COMMENT '用户出生日期',
  `user_detail_balance` decimal(10,2) NOT NULL COMMENT '用户余额（默认为0）',
  `user_detail_name` varchar(128) DEFAULT NULL COMMENT '姓名(个人身份实名认证可以为空，但购买商品需要实名认证，防止未成年人购买)',
  `user_detail_ID_number` varchar(18) DEFAULT NULL COMMENT '实名认证身份证号',
  `user_detail_intro` varchar(255) DEFAULT NULL COMMENT '个人简介',
  `user_detail_integral` bigint(20) NOT NULL DEFAULT '0' COMMENT '积分(默认为0)',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`user_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of user_detail
-- ----------------------------

-- ----------------------------
-- Table structure for warehouse
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse` (
  `warehouse_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) NOT NULL COMMENT '仓库id',
  `sku_id` varchar(20) NOT NULL COMMENT 'skuid',
  `stock` int(11) NOT NULL COMMENT '库存量',
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`warehouse_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='仓库中间表';

-- ----------------------------
-- Records of warehouse
-- ----------------------------

-- ----------------------------
-- Table structure for warehouse_detail
-- ----------------------------
DROP TABLE IF EXISTS `warehouse_detail`;
CREATE TABLE `warehouse_detail` (
  `warehouse_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) NOT NULL COMMENT '仓库id',
  `sku_id` varchar(20) NOT NULL COMMENT 'skuid',
  `stock` int(11) NOT NULL COMMENT '库存量',
  `location_x` decimal(10,0) NOT NULL,
  `location_y` decimal(10,0) NOT NULL,
  `raw_add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  `raw_update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`warehouse_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='仓库中间表';

-- ----------------------------
-- Records of warehouse_detail
-- ----------------------------
